
import { Link, Head } from '@inertiajs/react';
import React, { useState, useEffect } from 'react';

import './bs.min.css';
import axios from 'axios';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import DynamicForm from '@/Components/Helper/DynamicForm';
const Loanpage = ({auth}) => {

    const [formData, setFormData] = useState([]);
  const [initialFormData, setInitialFormData] = useState({}); // St


  useEffect(() => {
    axios
      .get('/loanset')
      .then((response) => {
        setFormData(response.data);
        setInitialFormData(response.data[0]); // Set the initial form data to the first item in the response array
        console.log('Fetched data:', response.data);
      })
      .catch((error) => {
        console.error('Error fetching admin users:', error);
      });
  }, []);


  const fields = [
    // {
    //   name: 'loanname',
    //   label: 'Page Name',
    //   type: 'text', // Use 'select' as the input type for the dropdown
    //   required: true,
     
    // },
    {
      name: 'des',
      label: 'Short Description',
      type: 'text',
      required: true,
    },
    {
        name: 'des1',
        label: 'Left Short Description ',
        type: 'text',
        required: true,
    },
    {
        name: 'des2',
        label: 'Right Short Description',
        type: 'text',
        required: true,
    },
    {
        name: 'des3',
        label: 'Last Description',
        type: 'text',
        required: true,
    },
    {
        name: 'des4',
        label: 'Get financing for whatever you need now 1',
        type: 'text',
        required: true,
    },
    {
        name: 'des5',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des6',
        label: 'Get financing for whatever you need now 2',
        type: 'text',
        required: true,
    },
    {
        name: 'des7',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    // Add more fields if needed
    
    {
        name: 'des8',
        label: 'Get financing for whatever you need now 3',
        type: 'text',
        required: true,
    },
    {
        name: 'des9',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des10',
        label: 'Get financing for whatever you need now 4',
        type: 'text',
        required: true,
    },
    {
        name: 'des11',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    {
        name: 'des12',
        label: 'Features of  Loan 1',
        type: 'text',
        required: true,
    },
    {
        name: 'des13',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des14',
        label: 'Features of  Loan 2',
        type: 'text',
        required: true,
    },
    {
        name: 'des15',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    // Add more fields if needed
    
    {
        name: 'des16',
        label: 'Features of  Loan 3',
        type: 'text',
        required: true,
    },
    {
        name: 'des17',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des18',
        label: 'Features of  Loan 4',
        type: 'text',
        required: true,
    },
    {
        name: 'des19',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    {
        name: 'des20',
        label: 'Loan - Eligibility 1',
        type: 'text',
        required: true,
    },
    {
        name: 'des21',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des22',
        label: 'Loan - Eligibility 2',
        type: 'text',
        required: true,
    },
    {
        name: 'des23',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    // Add more fields if needed
    
    {
        name: 'des24',
        label: 'Loan - Eligibility 3',
        type: 'text',
        required: true,
    },
    {
        name: 'des25',
        label: 'Description on that',
        type: 'text',
        required: true,
    },

    {
        name: 'des26',
        label: 'Loan - Eligibility 4',
        type: 'text',
        required: true,
    },
    {
        name: 'des27',
        label: 'Description on that',
        type: 'text',
        required: true,
    },
    // Add more fields if needed






    {
      name: 'id',
      label: '',
      type: 'hidden',
      required: true,
    },
  ];



  const handleChange = (e) => {
    const { name, value } = e.target;
    const selectedLoanData = formData.find((item) => item.id === parseInt(value, 10));
    setInitialFormData(selectedLoanData);
  };

  return (
   <>
   
   <div id="wrapper">

  <Dashboarsidebar auth={auth}/>
    
      <div className="d-flex flex-column" id="content-wrapper">
        <div id="content">

          
          <Dashboardheader auth={auth}/>
          <div className="container-fluid">
            <h3 className="text-dark mb-1">Loan Page Settings</h3>
          </div>
          <div className="container" style={{ marginTop: '50px' }}>
        {/* Render the dropdown */}

        <label htmlFor="loanname">Select Loan Products:</label>
        <select
          name="loanname"
          id="loanname"
         
          className="form-control"
          value={initialFormData.id || ''}
          onChange={handleChange}
        >
          {formData.map((item) => (
            <option key={item.id} value={item.id}>
              {item.loanname}
            </option>
          ))}
        </select>

        {/* <pre>{JSON.stringify(formData, null, 2)}</pre> */}
        <DynamicForm routeName="loansettingspageupdate" typep="post" fields={fields} initialData={initialFormData} />
        </div>

        </div>
       <Dashboardfooter/>
      </div>
      
    </div>
   
   </>
  )
}

export default Loanpage