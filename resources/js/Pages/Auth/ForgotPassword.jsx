import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, useForm } from '@inertiajs/react';
import Footer from '@/Components/Footer';
import Header from '@/Components/Header';

export default function ForgotPassword({ status ,auth }) {
  const { data, setData, post, processing, errors } = useForm({
    email: '',
  });

  const submit = (e) => {
    e.preventDefault();

    post(route('password.email'));
  };

  return (
    <>
        <Header auth={auth} />





      <div className="container" style={{marginTop:'200px',marginBottom:'150px'}}>
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="mb-4 text-sm text-gray-600">
              Forgot your password? No problem. Just let us know your email address, and we will email you a password reset link that will allow you to choose a new one.
            </div>

            {status && <div className="alert alert-success mb-4">{status}</div>}

            <form onSubmit={submit}>
              <div className="mb-3">
                <TextInput
                  id="email"
                  type="email"
                  name="email"
                  value={data.email}
                  className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                  isFocused={true}
                  onChange={(e) => setData('email', e.target.value)}
                />

                <InputError message={errors.email} className="invalid-feedback" />
              </div>

              <div className="d-grid gap-2">
                <PrimaryButton className="btn btn-primary" disabled={processing}>
                  Email Password Reset Link
                </PrimaryButton>
              </div>
            </form>
          </div>
        </div>
      </div>

      <Footer/>
    </>
  );
}
