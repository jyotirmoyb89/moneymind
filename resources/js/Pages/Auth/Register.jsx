import { useEffect } from 'react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';
import Header from '@/Components/Header';
import Footer from '@/Components/Footer';

export default function Register({ auth }) {
  const { data, setData, post, processing, errors, reset } = useForm({
    name: '',
    email: '',
    phone: '',
    password: '',
    password_confirmation: '',
  });

  useEffect(() => {
    return () => {
      reset('password', 'password_confirmation');
    };
  }, []);

  const submit = (e) => {
    e.preventDefault();
    post(route('register'));
  };

  return (
    <>
      
      <Header auth={auth} />


      <div className="container" style={{ marginTop: '150px', marginBottom: '50px' }}>
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="card">
              <div className="card-body">
                <h2 className="text-center mb-4">Signup</h2>
                <form onSubmit={submit}>
                  <div className="mb-3">


          <InputLabel htmlFor="name" value="Name" />

          <TextInput
            id="name"
            name="name"
            value={data.name}
            className="form-control"
            autoComplete="name"
            isFocused={true}
            onChange={(e) => setData('name', e.target.value)}
            required
          />

          <InputError message={errors.name} />
        </div>

        <div className="mb-3">
          <InputLabel htmlFor="email" value="Email" />

          <TextInput
            id="email"
            type="email"
            name="email"
            value={data.email}
            className="form-control"
            autoComplete="username"
            onChange={(e) => setData('email', e.target.value)}
            required
          />

          <InputError message={errors.email} />
        </div>


        <div className="mb-3">
          <InputLabel htmlFor="phone" value="Phone" />

          <TextInput
            id="phone"
            type="number"
            name="phone"
            value={data.phone}
            className="form-control"
            autoComplete="phone"
            onChange={(e) => setData('phone', e.target.value)}
            required
          />

          <InputError message={errors.phone} />
        </div>

        <div className="mb-3">
          <InputLabel htmlFor="password" value="Password" />

          <TextInput
            id="password"
            type="password"
            name="password"
            value={data.password}
            className="form-control"
            autoComplete="new-password"
            onChange={(e) => setData('password', e.target.value)}
            required
          />

          <InputError message={errors.password} />
        </div>

        <div className="mb-3">
          <InputLabel htmlFor="password_confirmation" value="Confirm Password" />

          <TextInput
            id="password_confirmation"
            type="password"
            name="password_confirmation"
            value={data.password_confirmation}
            className="form-control"
            autoComplete="new-password"
            onChange={(e) => setData('password_confirmation', e.target.value)}
            required
          />

          <InputError message={errors.password_confirmation} />
        </div>

        <div className="d-flex justify-content-end mt-4">
          <Link
            href={route('login')}
            className="text-decoration-underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 me-4"
          >
            Already registered?
          </Link>

          <PrimaryButton className="btn btn-primary" disabled={processing}>
            Register
          </PrimaryButton>
        </div>
      </form>
      </div>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </>
  );
}
