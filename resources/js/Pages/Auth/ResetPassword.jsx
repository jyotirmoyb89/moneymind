import { useEffect } from 'react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, useForm } from '@inertiajs/react';
import Header from '@/Components/Header';
import Footer from '@/Components/Footer';

export default function ResetPassword({ token, email,auth  }) {
  const { data, setData, post, processing, errors, reset } = useForm({
    token: token,
    email: email,
    password: '',
    password_confirmation: '',
  });

  useEffect(() => {
    return () => {
      reset('password', 'password_confirmation');
    };
  }, []);

  const submit = (e) => {
    e.preventDefault();

    post(route('password.store'));
  };

  return (
    <>
      <Head title="Reset Password" />

      <Header auth={auth} />


      <div className="container " style={{marginTop:'200px',marginBottom:'150px'}}>
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <form onSubmit={submit}>
              <div className="mb-3">
                <InputLabel htmlFor="email" value="Email" />

                <TextInput
                  id="email"
                  type="email"
                  name="email"
                  value={data.email}
                  className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                  autoComplete="username"
                  onChange={(e) => setData('email', e.target.value)}
                />

                <InputError message={errors.email} className="invalid-feedback" />
              </div>

              <div className="mb-3">
                <InputLabel htmlFor="password" value="Password" />

                <TextInput
                  id="password"
                  type="password"
                  name="password"
                  value={data.password}
                  className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                  autoComplete="new-password"
                  isFocused={true}
                  onChange={(e) => setData('password', e.target.value)}
                />

                <InputError message={errors.password} className="invalid-feedback" />
              </div>

              <div className="mb-3">
                <InputLabel htmlFor="password_confirmation" value="Confirm Password" />

                <TextInput
                  type="password"
                  name="password_confirmation"
                  value={data.password_confirmation}
                  className={`form-control ${
                    errors.password_confirmation ? 'is-invalid' : ''
                  }`}
                  autoComplete="new-password"
                  onChange={(e) => setData('password_confirmation', e.target.value)}
                />

                <InputError message={errors.password_confirmation} className="invalid-feedback" />
              </div>

              <div className="d-grid gap-2">
                <PrimaryButton className="btn btn-primary" disabled={processing}>
                  Reset Password
                </PrimaryButton>
              </div>
            </form>
          </div>
        </div>
      </div>

      <Footer/>
    </>
  );
}
