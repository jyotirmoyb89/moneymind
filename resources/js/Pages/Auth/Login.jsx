import { useEffect } from 'react';
import Checkbox from '@/Components/Checkbox';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';
import Header from '@/Components/Header';
import Footer from '@/Components/Footer';

export default function Login({ status, canResetPassword, auth }) {
  const { data, setData, post, processing, errors, reset } = useForm({
    email: '',
    password: '',
    remember: false,
  });

  useEffect(() => {
    return () => {
      reset('password');
    };
  }, []);

  const submit = (e) => {
    e.preventDefault();

    post(route('login'));
  };

  return (
    <>
      <Header auth={auth} />

      <div className="container" style={{ marginTop: '150px', marginBottom: '50px' }}>
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="card">
              <div className="card-body">
                <h2 className="text-center mb-4">Login</h2>
            {status && <div className="alert alert-success mb-4">{status}</div>}

            <form onSubmit={submit}>
              <div className="mb-4">
                <InputLabel htmlFor="email" value="Email" />

                <TextInput
                  id="email"
                  type="email"
                  name="email"
                  value={data.email}
                  className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                  autoComplete="username"
                  isFocused={true}
                  onChange={(e) => setData('email', e.target.value)}
                />

                <InputError message={errors.email} className="invalid-feedback" />
              </div>

              <div className="mb-4">
                <InputLabel htmlFor="password" value="Password" />

                <TextInput
                  id="password"
                  type="password"
                  name="password"
                  value={data.password}
                  className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                  autoComplete="current-password"
                  onChange={(e) => setData('password', e.target.value)}
                />

                <InputError message={errors.password} className="invalid-feedback" />
              </div>

              <div className="mb-4">
                <div className="form-check">
                  <Checkbox
                    name="remember"
                    checked={data.remember}
                    onChange={(e) => setData('remember', e.target.checked)}
                    className="form-check-input"
                  />
                  <label className="form-check-label text-sm text-gray-600">Remember me</label>
                </div>
              </div>

              <div className="d-grid gap-2">
                {canResetPassword && (
                  <Link href={route('password.request')} className="text-sm text-gray-600">
                    Forgot your password?
                  </Link>
                )}

                <PrimaryButton className="btn btn-primary" disabled={processing}>
                  Log in
                </PrimaryButton>

                <div className="mt-3 text-center">
                  <span className="text-sm text-gray-600">Not registered?</span>
                  <Link href={route('register')} className="ml-2 text-sm text-gray-900">
                    Register
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
        </div>
      </div>

      <Footer />
    </>
  );
}
