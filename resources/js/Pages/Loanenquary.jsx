import React, { useState } from 'react';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import Sildertabel from '@/Components/dashboardcompo/Sildertabel';


const Loanenquary = ({auth}) => {
    const headers = ['id', 'loantype', 'name','email','phn','city','created_at','Action'];
  return (
    <div id="wrapper">
    <Dashboarsidebar auth={auth} />

    <div className="d-flex flex-column" id="content-wrapper">
      <div id="content">
        <Dashboardheader auth={auth} />
        <div className="container-fluid">
          <h3 className="text-dark mb-1">View Loanenquary</h3>
        </div>

     

      <Sildertabel headers={headers} routs='getall/loanquaries' imgColumns='' delename='delloanquary' />
      </div>
      <Dashboardfooter />
    </div>
  </div>
  )
}

export default Loanenquary