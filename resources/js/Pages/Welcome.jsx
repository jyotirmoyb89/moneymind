
import Footer from '@/Components/Footer';
import Header from '@/Components/Header';
import Home from '@/Components/Home';
import Lender from '@/Components/Lender';
import { Link, Head } from '@inertiajs/react';

export default function Welcome({ auth }) {
    return (
        <>
            <Header auth={auth} />

            <Home/>
<Lender/>
          <Footer />
        </>
    );
}
