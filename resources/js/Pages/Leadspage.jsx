import { Link, Head } from '@inertiajs/react';
import React, { useState } from 'react';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import Sildertabel from '@/Components/dashboardcompo/Sildertabel';

const Leadspage = ({ auth }) => {

  const headers = ['id','loanid', 'name', 'email', 'phn','add','incometype','loan','monthlyincome','aadhar','pan', 'incomep','Final Decision','Action'];
  return (
   <>
   
   <div id="wrapper">

<Dashboarsidebar auth={auth}/>
  
    <div className="d-flex flex-column" id="content-wrapper">
      <div id="content">

        
        <Dashboardheader auth={auth}/>
        <div className="container-fluid">
          <h3 className="text-dark mb-1">Leads </h3>
        </div>

        {/* <Sildertabel headers={headers} routs={`${getall}/${applyloans}`} delename='sliderdett' /> */}
        <Sildertabel headers={headers} routs='getall/applyloans' delename='del' imgColumns={['aadhar', 'pan']} />

      </div>
     <Dashboardfooter/>
    </div>
    
  </div>
   
   
   </>
  )
}

export default Leadspage