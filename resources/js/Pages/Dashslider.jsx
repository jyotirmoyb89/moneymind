import React, { useState } from 'react';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import Sildertabel from '@/Components/dashboardcompo/Sildertabel';
import { makeRequest } from '../Components/Helper/axiosInstance';
import DynamicForm from '@/Components/Helper/DynamicForm';

const Dashslider = ({ auth }) => {
  const [showUploadForm, setShowUploadForm] = useState(false);
  

  const handleShowUploadForm = () => {
    setShowUploadForm(true);
  };
  const handleHideUploadForm = () => {
    setShowUploadForm(false);
  };

  const fields = [
    {
      name: 'photo',
      label: 'Image',
      type: 'file', // Set the input type to 'file' for image uploads
      multiple: false, // Set this to true if you want to allow multiple image uploads
      required: true,
    },
    {
      name: 'content',
      label: 'content',
      type: 'text', // Set the input type to 'text' for regular input fields
      required: true,
    },
    // Add more fields if needed
  ];
 
  const headers = ['id', 'content', 'img', 'Action'];
  

  return (
    <>
      <div id="wrapper">
        <Dashboarsidebar auth={auth} />

        <div className="d-flex flex-column" id="content-wrapper">
          <div id="content">
            <Dashboardheader auth={auth} />
            <div className="container-fluid">
              <h3 className="text-dark mb-1">Slider Settings</h3>
            </div>

            {/* Create a button to open the upload form */}
            <button onClick={handleShowUploadForm} className="btn btn-warning" style={{ marginLeft: '25px', marginTop: '25px' }}>
              Upload Image
            </button>

            {/* Form to upload image */}
            {showUploadForm && (
              <>
              <div className="container" style={{ marginTop: '40px' }}>
                
                <DynamicForm routeName='addslider' typep='post' fields={fields} />

              

              </div>

<button onClick={handleHideUploadForm} className="btn btn-warning" style={{ marginLeft: '25px' ,marginTop:'5px' }}>
Cancel
</button>
</>
            )}

          <Sildertabel headers={headers} routs='sliderdet' imgColumns={['img']} delename='sliderdett' />
          </div>
          <Dashboardfooter />
        </div>
      </div>
    </>
  );
};

export default Dashslider;
