import { Link, Head } from '@inertiajs/react';
import { useState } from 'react'; // Import useState hook

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';

const Dashboard = ({ auth }) => {
  const [sidebarOpen, setSidebarOpen] = useState(true); // State to control sidebar visibility

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };

  return (
    <>
     

    


<div id="wrapper">

  <Dashboarsidebar auth={auth}/>
    
      <div className="d-flex flex-column" id="content-wrapper">
        <div id="content">

          
          <Dashboardheader auth={auth}/>
          <div className="container-fluid">
            <h3 className="text-dark mb-1">Blank Page</h3>
          </div>


        </div>
       <Dashboardfooter/>
      </div>
      
    </div>



    </>
  );
};

export default Dashboard;
