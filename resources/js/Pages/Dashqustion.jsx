import React, { useState } from 'react';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import Sildertabel from '@/Components/dashboardcompo/Sildertabel';

const Dashqustion = ({auth}) => {
    const headers = ['id',  'name','email','phone','message','created_at','Action'];
    return (
      <div id="wrapper">
      <Dashboarsidebar auth={auth} />
  
      <div className="d-flex flex-column" id="content-wrapper">
        <div id="content">
          <Dashboardheader auth={auth} />
          <div className="container-fluid">
            <h3 className="text-dark mb-1">View Qustion</h3>
          </div>
  
       
  
        <Sildertabel headers={headers} routs='getall/contactsettings' imgColumns='' delename='delquary' />
        </div>
        <Dashboardfooter />
      </div>
    </div>
  )
}

export default Dashqustion