import { Link, Head } from '@inertiajs/react';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';

const UserDashboard = ({ auth }) => {

    const [tableData, setTableData] = useState([]);

    useEffect(() => {
        // Fetch table data from the API using Axios
        axios.get('getusepanelloan') // Replace '/api/sliderdata' with your actual API endpoint URL
          .then((response) => {
            setTableData(response.data);
            console.log(response.data);
          })
          .catch((error) => {
            console.error('Error fetching table data:', error);
          });
      }, []);


  return (
    <>
    
<div id="wrapper">

<Dashboarsidebar auth={auth}/>
  
    <div className="d-flex flex-column" id="content-wrapper">
      <div id="content">

        
        <Dashboardheader auth={auth}/>
        <div className="container-fluid">
          <h3 className="text-dark mb-1">Welcome</h3>
        </div>

    <div className='table-responsive' style={{marginTop:'50px'}}>
        <table className="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            {/* Add your table headers here */}
                                            <th>Loan id</th>
                                            <th>Type Of Loan</th>
                                            <th>Status</th>
                                            <th>Remarks</th>
                                            {/* Add more headers as needed */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {tableData.map((row) => (
                                            <tr key={row.id}>
                                                {/* Map over the tableData array and display each row's data */}
                                                <td style={{textAlign:'center'}}>{row.loanid}</td>
                                                <td style={{textAlign:'center'}}>{row.loan}</td>
                                                <td style={{ textAlign: 'center' }}>
                {row.status === 0 ? (
                    <span style={{ color: 'orange' }}>Processing</span>
                ) : row.status === 1 ? (
                    <span style={{ color: 'green' }}>Approved</span>
                ) : (
                    <span style={{ color: 'red' }}>Decline</span>
                )}
            </td>
                                                <td style={{ textAlign: 'center' }}>
                {row.status === 0 ? (
                    <span style={{ color: 'orange' }}>We are Processing Your Application</span>
                ) : row.status === 1 ? (
                    <span style={{ color: 'green' }}>Your Application is Approved <br/> Approval Ammount:-{row.ammount}/-<br/>Your Emi:-{row.emi}/-<br/>
                    
                    Total Emi :-{row.month} Months
                    </span>
                ) : (
                    <span style={{ color: 'red' }}>Decline</span>
                )}
            </td>
                                                
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>

                                </div>

      </div>
     <Dashboardfooter/>
    </div>
    
  </div>
    
    </>
  )
}

export default UserDashboard