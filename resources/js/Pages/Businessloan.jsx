import Applyloan from '@/Components/Applyloan'
import Commonpage from '@/Components/Commonpage'
import Footer from '@/Components/Footer'
import Header from '@/Components/Header'
import Lender from '@/Components/Lender'
import Slider from '@/Components/Slider'
import React from 'react'

const Businessloan = ({ auth }) => {
  return (
    <>
   <Header auth={auth} />

    <Slider/>

    
    <Commonpage name={'Business Loan'}/>

   <Applyloan name={'BusinessLoan'}/>



  <Lender/>



    <Footer/>

    </>
  )
}

export default Businessloan