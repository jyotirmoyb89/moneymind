import Applyloan from '@/Components/Applyloan'
import Commonpage from '@/Components/Commonpage'
import Footer from '@/Components/Footer'
import Header from '@/Components/Header'
import Lender from '@/Components/Lender'
import Slider from '@/Components/Slider'
import React from 'react'

const Personalloan = ({ auth }) => {
  return (
    <>
    <Header auth={auth} />

    <Slider/>
   
    <Commonpage name={'Personal Loan'} />

    <Applyloan name={'PersonalLoan'}/>
<Lender/>
    <Footer/>
    
    
    </>
  )
}

export default Personalloan