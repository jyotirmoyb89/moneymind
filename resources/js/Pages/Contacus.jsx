import Footer from '@/Components/Footer'
import Header from '@/Components/Header'
import React, { useState } from 'react';
import axios from 'axios';
import { Link, Head } from '@inertiajs/react';
const Contacus = ({ auth }) => {


    const [formData, setFormData] = useState({
        name: '',
        email: '',
        phone: '',
        message: '',
      });
    
      const [successMessage, setSuccessMessage] = useState('');
      const [errorMessage, setErrorMessage] = useState('');
    
      const handleFormSubmit = (e) => {
        e.preventDefault();
    
        // Send a POST request to the 'contactus' route
        axios.post('quary', formData)
          .then((response) => {
            // Handle the success response here
            console.log('Form submitted successfully:', response.data);
            setSuccessMessage('Your message has been submitted successfully. We will get in touch with you shortly.');
            setErrorMessage('');
            // Clear form data
            setFormData({
              name: '',
              email: '',
              phone: '',
              message: '',
            });
          })
          .catch((error) => {
            // Handle the error if any
            console.error('Error submitting form:', error);
            setErrorMessage('An error occurred while submitting your message. Please try again later.');
            setSuccessMessage('');
          });
      };
    
      const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
          ...formData,
          [name]: value,
        });
      };


    return (
        <>
           <Header auth={auth} />

            <div className="page-header">
                <div className="container" style={{ marginTop: window.innerWidth <= 576 ? '120px' :'' }}>
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            {/* <div className="page-breadcrumb">
                                <ol className="breadcrumb">
                                    <li><a href="index.html">Home</a></li>
                                    <li className="active">Contact us</li>
                                </ol>
                            </div> */}
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="bg-white pinside30">
                                <div className="row">
                                    <div className="col-xl-4 col-lg-4 col-md-9 col-sm-12 col-12">
                                        <h1 className="page-title">Contact Us</h1>
                                    </div>
                                    <div className="col-xl-8 col-lg-8 col-md-3 col-sm-12 col-12">
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                {/* <div className="btn-action"> <a href="#" className="btn btn-default">How To Apply</a> </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="sub-nav" id="sub-nav">
                                <ul className="nav nav-justified">
                                    <li className="nav-item">
                                        <Link href="contactus" className="nav-link">Give me call back</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="loancalculator" className="nav-link">Emi Caculator</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="wrapper-content bg-white pinside40">
                            <div className="contact-form mb60">
                                <div className=" ">
                                    <div className="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <div className="mb60  section-title text-center  ">

                                            <h1>Get In Touch</h1>
                                            <p>Reach out to us &amp; we will respond as soon as we can.</p>
                                        </div>
                                    </div>

                                    <form className="contact-us" onSubmit={handleFormSubmit}>
      <div className="container">
        <div className="row">
          <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div className="form-group">
              <label className="sr-only control-label" htmlFor="name">Name</label>
              <input
                id="name"
                name="name"
                type="text"
                placeholder="Name"
                className="form-control input-md"
                value={formData.name}
                onChange={handleInputChange}
                required
              />
            </div>
          </div>

          <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div className="form-group">
              <label className="sr-only control-label" htmlFor="email">Email</label>
              <input
                id="email"
                name="email"
                type="email"
                placeholder="Email"
                className="form-control input-md"
                value={formData.email}
                onChange={handleInputChange}
                required
              />
            </div>
          </div>

          <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div className="form-group">
              <label className="sr-only control-label" htmlFor="phone">Phone</label>
              <input
                id="phone"
                name="phone"
                type="text"
                placeholder="Phone"
                className="form-control input-md"
                value={formData.phone}
                onChange={handleInputChange}
                required
              />
            </div>
          </div>

          <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div className="form-group">
              <label className="control-label" htmlFor="message">Message</label>
              <textarea
                className="form-control"
                id="message"
                rows="7"
                name="message"
                placeholder="Message"
                value={formData.message}
                onChange={handleInputChange}
                required
              ></textarea>
            </div>
          </div>

          <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <button type="submit" className="btn btn-default">Submit</button>
          </div>
        </div>
      </div>
      
      {successMessage && (
        <div className="alert alert-success mt-3">
          {successMessage}
        </div>
      )}

      {errorMessage && (
        <div className="alert alert-danger mt-3">
          {errorMessage}
        </div>
      )}
    </form>
                                </div>
                            </div>

                        </div>
                        <div className="contact-us mb60">
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="mb60  section-title">

                                        <h1>We are here to help you </h1>
                                        <p className="lead">Various versions have evolved over the years sometimes by accident sometimes on purpose injected humour and the like.</p>
                                    </div>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div className="bg-boxshadow pinside60 outline text-center mb30">
                                        <div className="mb40"><i className="icon-briefcase icon-2x icon-default"></i></div>
                                        <h2 className="capital-title">Branch Office</h2>
                                        <p>2843 Lakewood Drive
                                            <br /> Jersey City, CA 07304</p>
                                    </div>
                                </div>
                                <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div className="bg-boxshadow pinside60 outline text-center mb30">
                                        <div className="mb40"><i className="icon-phone-call icon-2x icon-default"></i></div>
                                        <h2 className="capital-title">Call us at </h2>
                                        <h1 className="text-big">800-123-456 / 789 </h1>
                                    </div>
                                </div>
                                <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div className="bg-boxshadow pinside60 outline text-center mb30">
                                        <div className="mb40"> <i className="icon-letter icon-2x icon-default"></i></div>
                                        <h2 className="capital-title">Email Address</h2>
                                        <p>lnfo@loanadvisor.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        
                        <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14731.177727067889!2d88.43069445!3d22.6241504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f89e31bdc08491%3A0xc5041e3b18a9b07a!2sCharnock%20Hospital!5e0!3m2!1sen!2sin!4v1691238338769!5m2!1sen!2sin"
        width="100%"
        height="450"
        style={{ border: 0 ,marginBottom:'50px' }}
        allowFullScreen
        loading="lazy"
      ></iframe> 


                  
                    </div>
                </div>
            </div>



            <Footer />


        </>
    )
}

export default Contacus