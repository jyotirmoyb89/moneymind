import Footer from '@/Components/Footer'
import Header from '@/Components/Header'
import DynamicForm from '@/Components/Helper/DynamicForm'
import React from 'react'

const Apply = ({ auth }) => {

    const fields = [
        {
          name: 'name',
          label: 'Your Name',
          type: 'text', // Use 'select' as the input type for the dropdown
          required: true,
         
        },
       
        
        {
            name: 'add',
            label: 'Your Address',
            type: 'text',
            required: true,
        },
        {
            name: 'loan', // Note: You can keep the field name the same as in your original code
            label: 'Select Type Of Loan',
            type: 'select', // Use 'select' as the input type for the dropdown
            required: true,
            options: [ // Add the options for the dropdown
                { value: 'personal', label: 'Personal Loan' },
                { value: 'home', label: 'Home Loan' },
                { value: 'business', label: 'Business Loan' },
                { value: 'loanagainstpropaty', label: 'Loan Against Propaty' },
                // Add more options as needed
            ],
        },
        {
            name: 'incometype', // Note: You can keep the field name the same as in your original code
            label: 'Type Of Income',
            type: 'select', // Use 'select' as the input type for the dropdown
            required: true,
            options: [ // Add the options for the dropdown
                { value: 'selfemployyed', label: 'Self Employeed' },
                { value: 'salaried', label: 'Salaried' }
                
                // Add more options as needed
            ],
        },

        {
            name: 'monthlyincome',
            label: 'Your Monthly Income',
            type: 'number',
            required: true,
        },
        
        {
            name: 'aadhar',
            label: 'Upload Aadhar Card',
            type: 'file', // Set the input type to 'file' for image uploads
            multiple: false, // Set this to true if you want to allow multiple image uploads
            required: true,
          },

          {
            name: 'pan',
            label: 'Upload PAN Card',
            type: 'file', // Set the input type to 'file' for image uploads
            multiple: false, // Set this to true if you want to allow multiple image uploads
            required: true,
          },


          {
            name: 'incomep',
            label: 'Upload Income Prof',
            type: 'file', // Set the input type to 'file' for image uploads
            multiple: false, // Set this to true if you want to allow multiple image uploads
            required: true,
          },
       
      ];


  return (
    <>
      <Header auth={auth} />

    <div className="page-header">
    <div className="container" style={{ marginTop: window.innerWidth <= 576 ? '200px' :'' }}>
            <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                   
                </div>
               <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div className="bg-white pinside30">
                        <div className="row">
                           <div className="col-xl-4 col-lg-4 col-md-9 col-sm-12 col-12">
                                <h1 className="page-title">Loan Eligibility</h1>
                            </div>
                            <div className="col-xl-8 col-lg-8 col-md-3 col-sm-12 col-12">
                                <div className="row">
                                       <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="sub-nav" id="sub-nav">
                         <ul className="nav nav-justified">
                            <li className="nav-item">
                                <a href="contactus" className="nav-link">Give me call back</a>
                            </li>
                            <li className="nav-item">
                                <a href="loancalculator" className="nav-link">Emi Caculator</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
   
   
        <div className="container">
        <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">     <div className="wrapper-content bg-white pinside40">
                    <div className="loan-eligibility-block">
                        
                            <div className="col-lg-6">
                                <h2 className="mb40">How much loan you are eligible for?</h2>
                                <div className="loan-eligibility-info">
                                 <DynamicForm fields={fields} routeName="applynowloan" typep="post"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
   
   




    <Footer/>
    
    
    </>
  )
}

export default Apply