import React, { useState } from 'react';

import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import Sildertabel from '@/Components/dashboardcompo/Sildertabel';
import { makeRequest } from '../Components/Helper/axiosInstance';
import DynamicForm from '@/Components/Helper/DynamicForm';

const Lendersettings = ({ auth }) => {

    const [showUploadForm, setShowUploadForm] = useState(false);
  

    const handleShowUploadForm = () => {
      setShowUploadForm(true);
    };
    const handleHideUploadForm = () => {
      setShowUploadForm(false);
    };

    const fields = [
        {
          name: 'photo',
          label: 'Upload Lenders images',
          type: 'file', // Set the input type to 'file' for image uploads
          multiple: false, // Set this to true if you want to allow multiple image uploads
          required: true,
        }
       
        // Add more fields if needed
      ];

      const headers = ['id', 'img', 'Action'];

  return (
   <> 
    <div id="wrapper">
        <Dashboarsidebar auth={auth} />

        <div className="d-flex flex-column" id="content-wrapper">
          <div id="content">
            <Dashboardheader auth={auth} />
            <div className="container-fluid">
              <h3 className="text-dark mb-1">Lender Settings</h3>
            </div>

            {/* Create a button to open the upload form */}
            <button onClick={handleShowUploadForm} className="btn btn-warning" style={{ marginLeft: '25px', marginTop: '25px' }}>
              Upload Image
            </button>

            {/* Form to upload image */}
            {showUploadForm && (
              <>
              <div className="container" style={{ marginTop: '40px' }}>
                
                <DynamicForm routeName='lenderpost' typep='post' fields={fields} />

              

              </div>

<button onClick={handleHideUploadForm} className="btn btn-warning" style={{ marginLeft: '25px' ,marginTop:'5px' }}>
Cancel
</button>
</>
            )}

          <Sildertabel headers={headers} routs='getall/landers' imgColumns={['img']} delename='dellenderpost' />
          </div>
          <Dashboardfooter />
        </div>
      </div>
   
   </>
  )
}

export default Lendersettings