import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';
import './bs.min.css';
import Dashboarsidebar from '@/Components/Dashboarsidebar';
import Dashboardheader from '@/Components/Dashboardheader';
import Dashboardfooter from '@/Components/Dashboardfooter';
import DynamicForm from '@/Components/Helper/DynamicForm';

const Homesettings = ({ auth }) => {
  const [formData, setFormData] = useState([]);
  const [initialFormData, setInitialFormData] = useState({}); // State to hold the initial form data

  useEffect(() => {
    axios
      .get('/hmst')
      .then((response) => {
        setFormData(response.data);
        setInitialFormData(response.data[0]); // Set the initial form data to the first item in the response array
        console.log('Fetched data:', response.data);
      })
      .catch((error) => {
        console.error('Error fetching admin users:', error);
      });
  }, []);

  const fields = [
    {
      name: 'loanname',
      label: 'Loan Name',
      type: 'text', // Use 'select' as the input type for the dropdown
      required: true,
     
    },
    {
      name: 'des',
      label: 'Short Description',
      type: 'text',
      required: true,
    },
    {
        name: 'per',
        label: 'Interest %',
        type: 'text',
        required: true,
    },
    // Add more fields if needed
    {
      name: 'id',
      label: '',
      type: 'hidden',
      required: true,
    },
  ];

  const handleChange = (e) => {
    const { name, value } = e.target;
    const selectedLoanData = formData.find((item) => item.id === parseInt(value, 10));
    setInitialFormData(selectedLoanData);
  };

  return (
    <>
       <div id="wrapper">

<Dashboarsidebar auth={auth}/>
  
    <div className="d-flex flex-column" id="content-wrapper">
      <div id="content">

        
        <Dashboardheader auth={auth}/>
        <div className="container-fluid">
          <h3 className="text-dark mb-1">Home Settings</h3>
        </div>


      <div className="container" style={{ marginTop: '50px' }}>
        {/* Render the dropdown */}

        <label htmlFor="loanname">Select Loan Products:</label>
        <select
          name="loanname"
          id="loanname"
         
          className="form-control"
          value={initialFormData.id || ''}
          onChange={handleChange}
        >
          {formData.map((item) => (
            <option key={item.id} value={item.id}>
              {item.loanname}
            </option>
          ))}
        </select>

        {/* Pass the initialFormData to the DynamicForm */}
        <DynamicForm routeName="homesettingsupdate" typep="post" fields={fields} initialData={initialFormData} />
        </div>

      </div>
     <Dashboardfooter/>
    </div>
    
  </div>
    </>
  );
};

export default Homesettings;
