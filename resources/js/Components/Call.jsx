import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';
const Call = () => {

    const [adminUsers, setAdminUsers] = useState([]);

    useEffect(() => {
        // Fetch the data from the Laravel API
        axios.get('/user')
          .then(response => {
            setAdminUsers(response.data);
          })
          .catch(error => {
            console.error('Error fetching admin users:', error);
          });
      }, []);
    



  return (
   <>
   
   <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div className="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div className="mb40"><i className="icon-calendar-3 icon-2x icon-default"></i></div>
                        <h2 className="capital-title">Apply For Loan</h2>
                        <p>Looking to buy a car or home loan? then apply for loan now.</p>
                        <Link href="contactus" className="btn-link">Get Appointment</Link> </div>
                </div>
                 <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div className="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div className="mb40"><i className="icon-phone-call icon-2x icon-default"></i></div>
                        <h2 className="capital-title">Call us at </h2>

                        {adminUsers.map(item => (
  <React.Fragment key={item.id}>
    <h1 className="text-big">{item.phone}</h1>
    <p>{item.email}</p>
  </React.Fragment>
))}

                        <Link href="contactus" className="btn-link">Contact us</Link> </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div className="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div className="mb40"> <i className="icon-users icon-2x icon-default"></i></div>
                        <h2 className="capital-title">Talk to Advisor</h2>
                        <p>Need to loan advise? Talk to our Loan advisors.</p>
                        <Link href="contactus" className="btn-link">Meet The Advisor</Link> </div>
                </div>
            </div>
   
   </>
  )
}

export default Call