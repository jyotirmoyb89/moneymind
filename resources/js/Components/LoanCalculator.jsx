import React, { useEffect, useState } from 'react';
import InputSlider from 'react-input-slider';

const LoanCalculator = () => {
  const [loanAmount, setLoanAmount] = useState(300000);
  const [numMonths, setNumMonths] = useState(36);
  const [rateOfInterest, setRateOfInterest] = useState(15);
  const [monthlyEMI, setMonthlyEMI] = useState(0);
  const [totalInterest, setTotalInterest] = useState(0);
  const [payableAmount, setPayableAmount] = useState(0);
  const [interestPercentage, setInterestPercentage] = useState(0);

  useEffect(() => {
    calculateLoan();
  }, [loanAmount, numMonths, rateOfInterest]);

  const calculateLoan = () => {
    const roi = rateOfInterest / 1200;
    const emi =
      (loanAmount * roi * Math.pow(1 + roi, numMonths)) /
      (Math.pow(1 + roi, numMonths) - 1);
    setMonthlyEMI(emi.toFixed(2));

    const totalInt = emi * numMonths - loanAmount;
    setTotalInterest(totalInt.toFixed(2));

    setPayableAmount((parseFloat(loanAmount) + parseFloat(totalInt)).toFixed(2));

    setInterestPercentage(((totalInt / loanAmount) * 100).toFixed(2));
  };

  return (
    <div className="container"style={{marginTop:'40px',marginBottom:'50px'}}>
      <div className="bg-light pinside40 outline">
        <span>Loan Amount &nbsp; </span>
        <strong>
          <span className="pull-right" id="la_value">
            {loanAmount}
          </span>
        </strong>
        <InputSlider
          axis="x"
          xstep={10000}
          xmin={100000}
          xmax={5000000}
          x={loanAmount}
          onChange={({ x }) => setLoanAmount(x)}
        />
      </div>

      <div className="bg-light pinside40 outline">
        <span>
          No. of Month &nbsp; {' '}
          <strong>
            <span className="pull-right" id="nm_value">
              {numMonths}
            </span>
          </strong>
        </span>
        <InputSlider
          axis="x"
          xstep={1}
          xmin={12}
          xmax={360}
          x={numMonths}
          onChange={({ x }) => setNumMonths(x)}
        />
      </div>

      <div className="bg-light pinside40 outline">
        <span>
          Rate of Interest [ROI] &nbsp; &nbsp; {' '}
          <strong>
            <span className="pull-right" id="roi_value">
              {rateOfInterest}
            </span>
          </strong>
        </span>
        <InputSlider
          axis="x"
          xstep={0.05}
          xmin={8}
          xmax={16}
          x={rateOfInterest}
          onChange={({ x }) => setRateOfInterest(x)}
        />
      </div>

      <div className="bg-light pinside30 outline">
        Monthly EMI
        <h2 id="emi" className="pull-right">
          {monthlyEMI}
        </h2>
      </div>

      <div className="bg-light pinside30 outline">
        Total Interest
        <h2 id="tbl_int" className="pull-right">
          {totalInterest}
        </h2>
      </div>

      <div className="bg-light pinside30 outline">
        Payable Amount
        <h2 id="tbl_full" className="pull-right">
          {payableAmount}
        </h2>
      </div>

      <div className="bg-light pinside30 outline">
        Interest Percentage
        <h2 id="tbl_int_pge" className="pull-right">
          {interestPercentage}
        </h2>
      </div>

      <div className="table-responsive">
        <table className="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Installment Number</th>
              <th>Principal Amount</th>
              <th>Interest Amount</th>
              <th>Total Payment</th>
              <th>Balance</th>
            </tr>
          </thead>
          <tbody>
            {Array.from({ length: numMonths }).map((_, index) => {
              const principal = (monthlyEMI * Math.pow(1 + rateOfInterest / 1200, index)) - ((monthlyEMI * Math.pow(1 + rateOfInterest / 1200, index - 1)) || 0);
              const interest = (monthlyEMI * Math.pow(1 + rateOfInterest / 1200, index - 1)) || 0;
              const totalPayment = principal + interest;
              const balance = payableAmount - totalPayment * (numMonths - index);
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{principal.toFixed(2)}</td>
                  <td>{interest.toFixed(2)}</td>
                  <td>{totalPayment.toFixed(2)}</td>
                  <td>{balance.toFixed(2)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    
    </div>
  );
};

export default LoanCalculator;
