import React, { useState } from 'react';
import axios from 'axios';

const Applyloan = (props) => {

   
        const [formData, setFormData] = useState({
          name: '',
          email: '',
          phn: '',
          city: '',
          loantype: props.name,
        });

        const [successMessage, setSuccessMessage] = useState('');
        const [errorMessage, setErrorMessage] = useState('');
    
        const handleFormSubmit = (e) => {
          e.preventDefault();
      
          // Send a POST request to the 'loanquary' route
          axios.post('loanquary', formData)
            .then((response) => {
              // Handle the success response here
              console.log('Request submitted successfully:', response.data);
              setSuccessMessage('Your request has been submitted successfully. An agent will call you shortly.');
              setErrorMessage('');
            })
            .catch((error) => {
              // Handle the error if any
              console.error('Error submitting request:', error);
              setErrorMessage('An error occurred while submitting your request. Please try again later.');
        setSuccessMessage('');

            });
        };
      
        const handleInputChange = (e) => {
          const { name, value } = e.target;
          setFormData({
            ...formData,
            [name]: value,
          });
        };

        
  return (
    <>
     <div className="section-space80  call-to-action" style={{
          background: 'linear-gradient(to right, #ff8a00, #da1b60)',
        }}>
        <div className="container">
            <div className="row">
               <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                    <div className="">
                       
                        <h1 className="text-white">Get Started Simple & Securely</h1>
                        <p className="text-white">Donec ullamcorper magna non orci scelerisque consectetur. Aenean ornare lectus nunc, elementum fermentum erat mattis id.</p>
                        <p className="text-white">Aenean ornare lectus nunc, elementum fermentum erat mattis id. Donec ullamcorper magna non orci scelerisque consectetur. </p>
                            <h3 className="text-white">Just 3 step away</h3>
                        <ul className="listnone">
                            <li>1. Fill the online form. Apply Now</li>
                            <li>2. Fill in the mandatory details, & click Submit</li>
                            <li>3. Our representative will get in touch with you</li>
                        </ul>
                    </div>
                    
                </div>
                <div className="offset-xl-2 col-lg-5 offset-lg-2 col-lg-5 col-md-6 col-sm-12 col-12">
                    <div className="business-request-form well-box">
                            <h3>Let us help your business grow!</h3>
                            <form onSubmit={handleFormSubmit}>
  <div className="row">
    <div className="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <label className="control-label sr-only" htmlFor="name">Name</label>
      <input
        id="name"
        name="name"
        type="text"
        placeholder="Name"
        className="form-control input-md"
        value={formData.name}
        onChange={handleInputChange}
        required
      />
    </div>

    <div className="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <label className="control-label sr-only" htmlFor="email">E-Mail</label>
      <input
        id="email"
        name="email"
        type="email"
        placeholder="E-mail"
        className="form-control input-md"
        value={formData.email}
        onChange={handleInputChange}
        required
      />
    </div>

    <div className="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <label className="control-label sr-only" htmlFor="phone">Phone</label>
      <input
        id="phone"
        name="phn"
        type="tel"
        placeholder="Phone"
        className="form-control input-md"
        value={formData.phn}
        onChange={handleInputChange}
        required
      />
    </div>

    <div className="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <label className="control-label sr-only" htmlFor="city">City</label>
      <input
        id="city"
        name="city"
        type="text"
        placeholder="City"
        className="form-control input-md"
        value={formData.city}
        onChange={handleInputChange}
        required
      />
    </div>

    <div className="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <button type="submit" className="btn btn-default btn-block">Send A Request</button>
    </div>
  </div>



  
</form>



{successMessage && (
        <div className="alert alert-success mt-3">
          {successMessage}
        </div>
      )}

      {errorMessage && (
        <div className="alert alert-danger mt-3">
          {errorMessage}
        </div>
)}


                    </div>
                </div>
            </div>
        </div>
    </div>
    
    </>
  )
}

export default Applyloan