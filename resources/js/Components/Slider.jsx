import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';

const Slider = () => {
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    // Fetch table data from the API using Axios
    axios.get('getdataall/sliderdatas') // Replace with your actual API endpoint URL
      .then((response) => {
        setTableData(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  }, []);

  return (
    <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel" style={{ marginTop: window.innerWidth <= 576 ? '120px' :'134px' }}>
      <div className="carousel-inner">
        {tableData.map((row, index) => (
          <div key={index} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
            <img src={row.img} className="d-block w-100 slider-image slider-overlay" style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: window.innerWidth <= 576 ? '200px' : '534px', objectFit: 'cover' }} alt="Slider Image" />
            <div className="slider-overlay" style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}></div>
            <div className="container">
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div className="slider-captions">
                    <h1 className="slider-title">{row.content}</h1>
                    <p className="slider-text d-none d-xl-block d-lg-block d-sm-block">The low rate you need for the need you want!</p>
                    {/* Uncomment the line below if you want to show a phone number */}
                    {/* <strong className="text-highlight">(555) 123-4567</strong></p> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
};

export default Slider;
