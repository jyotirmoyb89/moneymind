import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Lender = () => {
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    // Fetch table data from the API using Axios
    axios.get('getdataall/landers') // Replace with your actual API endpoint URL
      .then((response) => {
        setTableData(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  }, []);

  return (
    <>
      <div className="section-space140">
        <div className="container-fluid">
          <div className="row">
            <div className="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
              <div className="section-title mb60 text-center">
                <h1>Meet our lenders</h1>
                <p>Compare 60+ business funding options & check eligibility saving you time/money</p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div
                className="lender-container"
                style={{
                  display: 'flex',
                  overflowX: 'hidden',
                  width: '100%', // Adjust the width based on image count
                }}
              >
                {tableData.map((row, index) => (
                  <div
                    key={index}
                    className="lender-block bg-boxshadow"
                    style={{
                      flex: '0 0 auto',
                      marginRight: '10px', // Reduced the margin for better spacing
                      animation: 'scrollLenders 20s linear infinite',
                    }}
                  >
                    <img src={row.img} alt="" style={{ height: '50px', width: '150px', objectFit: 'cover' }} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>
        {`
        @keyframes scrollLenders {
          0% {
            transform: translateX(0);
          }
          100% {
            transform: translateX(calc(-100% - 10px));
          }
        }
      `}
      </style>
    </>
  );
};

export default Lender;
