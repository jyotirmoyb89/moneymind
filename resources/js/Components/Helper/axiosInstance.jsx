import axios from 'axios';

const instance = axios.create({
   // Replace this with your API base URL
  headers: {
    
    "Content-Type": "multipart/form-data",
    // Add any other common headers you might need for all requests
  },
});

// Define a function to make a dynamic API request
export const makeRequest = async (method, routeName, data) => {
  try {
    const response = await instance({
      method, // Use the dynamic method (e.g., 'post', 'patch', 'get', etc.)
      url: routeName,
      data,
    });
    return response.data; // Return the response data from the API
  } catch (error) {
    throw error; // Throw an error if the request fails
  }
};

// You can define other functions for different types of requests (e.g., POST, PATCH, GET, etc.) in a similar way.
