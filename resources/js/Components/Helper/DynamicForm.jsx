import React, { useState, useEffect } from 'react';
import { makeRequest } from './axiosInstance'; // Import the makeRequest function

const DynamicForm = ({ routeName, fields = [], initialData, typep })  => {
  const [formData, setFormData] = useState({});
  const [responseMessage, setResponseMessage] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    // Set initial form data if provided
    if (initialData) {
      setFormData(initialData);
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;
    const fieldValue = type === 'file' ? e.target.files[0] : value;

    // If the field type is 'file' and the user has selected a file, update the form data with the file object
    if (type === 'file' && files.length > 0) {
      setFormData((prevFormData) => ({ ...prevFormData, [name]: files[0] }));
    } else {
      setFormData((prevFormData) => ({ ...prevFormData, [name]: fieldValue }));
    }

    // Log the input
    console.log('Input Name:', name);
    console.log('Input Value:', fieldValue);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);
    setResponseMessage('');

    try {
      const formDataObj = new FormData();
      // Append the form data to the FormData object
      Object.keys(formData).forEach((key) => {
        formDataObj.append(key, formData[key]);
      });

      const responseData = await makeRequest(typep, route(routeName), formDataObj); // Call the makeRequest function with the dynamic method and route name
      console.log(responseData); // Handle the response data from the API
      setResponseMessage('Form submitted successfully!');
      window.location.reload();
    } catch (error) {
      // Handle the error if any
      console.error(error);
      setResponseMessage('Error submitting form. Please try again.');
    } finally {
      setIsSubmitting(false);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit} encType="multipart/form-data">
        {fields.map((field) => (
          <div key={field.name} className="mb-3">
            <label htmlFor={field.name} className="form-label">
              {field.label}
            </label>
            {field.type === 'select' ? (
              <select
                name={field.name}
                id={field.name}
                className="form-control"
                value={formData[field.name] || ''}
                onChange={handleChange}
                required={field.required}
              >
                <option value="">Select {field.label}</option>
                {field.options &&
                  field.options.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
              </select>
            ) : field.type === 'file' ? (
              field.multiple ? (
                <input
                  type="file"
                  name={field.name}
                  id={field.name}
                  className="form-control"
                  onChange={handleChange}
                  multiple
                />
              ) : (
                <input
                  type="file"
                  name={field.name}
                  id={field.name}
                  className="form-control"
                  onChange={handleChange}
                />
              )
            ) : (
              <input
                type={field.type || 'text'}
                name={field.name}
                id={field.name}
                className="form-control"
                value={formData[field.name] || ''}
                onChange={handleChange}
                required={field.required}
              />
            )}
          </div>
        ))}
        <div>
          <button type="submit" className="btn btn-warning" disabled={isSubmitting}>
            Submit
          </button>
        </div>
      </form>
      {responseMessage && <p>{responseMessage}</p>}
    </div>
  );
};

export default DynamicForm;
