import React, { useState } from 'react';
import { Link, Head } from '@inertiajs/react';





const Header = ({ auth }) => {

  




  return (
    <>

<style>
        {`
          .nav-link {
            color: lightgray; /* Set default color to gray */
            font-weight: bold; /* Make the font bold */
          }
          
          .nav-link:hover {
            color: lightgreen !important; /* Change color on hover to light green */
          }

          .dropdown-item{
            color: lightgray; /* Set default color to gray */
            font-weight: bold; /* Make the font bold */
          }

          .dropdown-item:hover{

            color: lightgreen !important;
          }

        `}
      </style>


      <div className="collapse searchbar" id="searchbar">
        <div className="search-area">
          <div className="container">
            <div className="row">
              <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div className="input-group">
                  <input type="text" className="form-control" placeholder="Search for..." />
                  <span className="input-group-btn">
                    <button className="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='fixed-top'>
        <div className="top-bar">
          <div className="container">
            <div className="row">
              <div className="col-xl-4 col-lg-5 col-md-4 col-sm-6 col-6 d-none d-xl-block d-lg-block">
                <p className="mail-text">Welcome to our Borrow Loan Website Templates</p>
              </div>
              <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 d-none d-xl-block d-lg-block">
                <p className="mail-text text-center">Call us at 1-800-123-4567</p>
              </div>
              <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 d-none d-xl-block d-lg-block">
                <p className="mail-text text-center">Mon to fri 10:00am - 06:00pm</p>
              </div>
              <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 d-none d-xl-block d-lg-block">
                <p className="mail-text text-center">Get started today</p>
              </div>
            </div>
          </div>
        </div>
        
        

<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container"style={{marginTop:'10px'}}> 
    <Link href="/" className="navbar-brand"><img src="/assets/images/logo-black.svg" alt="Borrow - Loan Company Website Template" style={{width:'200px'}} /></Link>
    
                
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav ms-auto mb-2 mb-lg-0"> 
        <li className="nav-item">
          <Link className="nav-link" aria-current="page"  href="/">Home</Link>
        </li>
        
        <li className="nav-item dropdown">
          <Link className="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Loan Product
          </Link>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><Link className="dropdown-item" href="homeloan">Home Loan</Link></li>
            <li><Link className="dropdown-item" href="businessloan">Business Loan</Link></li>
            
            <li><Link className="dropdown-item" href="personalloan">Personal Loan</Link></li>
            <li><Link className="dropdown-item" href="loanagainstproperty">Loan Against Property</Link></li>
          </ul>
        </li>
        <li className="nav-item">
          <Link className="nav-link" href="about">About us</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" href="faq">FAQ</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" href="loancalculator">Loan Calculator</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" href="contactus">Contac Us</Link>
        </li>

        <li className="nav-item">

        {auth.user ? (
          <Link className="nav-link" href="dashboard">Dashboard</Link>
          ):(
            <>
            
            <Link className="nav-link" href="login">Login</Link>
            </>
          )}
          </li>
      </ul>
    </div>
  </div>
</nav>





      </div>

     
    </>
  )
}

export default Header