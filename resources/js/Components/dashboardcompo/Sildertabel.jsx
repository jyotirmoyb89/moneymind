import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, Head } from '@inertiajs/react';

const Sildertabel = ({ headers, routs,delename ,imgColumns}) => {
  const [tableData, setTableData] = useState([]);

  const [showApproveModal, setShowApproveModal] = useState(false);
const [selectedLoanId, setSelectedLoanId] = useState('');
const [approvedAmount, setApprovedAmount] = useState('');
const [Emi, setEmi] = useState('');
const [Month, setMonth] = useState('');

  useEffect(() => {
    // Fetch table data from the API using Axios
    axios.get(routs) // Replace '/api/sliderdata' with your actual API endpoint URL
      .then((response) => {
        setTableData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  }, [routs]);


  const fetchData = () => {
    axios.get(routs) // Fetch data again after approval
      .then((response) => {
        setTableData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  };

  const handleDelete = (id) => {
    // Send a DELETE request to the delete route with the given ID
    axios.get(`${delename}/${id}`)
      .then((response) => {
        // Handle the success response here (e.g., show a success message)
        console.log('Delete request successful', response);
      })
      .catch((error) => {
        // Handle the error if any
        console.error('Error deleting data:', error);
      });
  };
  const handleApprove = (id) => {
    setApprovedAmount('');
  setShowApproveModal(true);
  setSelectedLoanId(id);
   
  };


  const handleApproveFormSubmit = (e) => {
    e.preventDefault();
  
    // Send the approved amount and id to the backend using the loanupdate route
    axios.post('approve', {
      id: selectedLoanId,
      approvedAmount: approvedAmount,
      Emi: Emi,
      Month: Month,
    })
    .then((response) => {
      // Handle the success response here (e.g., close the modal, update table)
      setShowApproveModal(false);
      fetchData();
      // Perform any other necessary actions, such as updating the table data
    })
    .catch((error) => {
      // Handle the error if any
      console.error('Error updating loan:', error);
    });
  };



  const handleDecline = (id) => {
    // Send a DELETE request to the delete route with the given ID
    axios.get(`decline/${id}`)
      .then((response) => {
        // Handle the success response here (e.g., show a success message)
        console.log('Delete request successful', response);
        // fetchData();

      })
      
      .catch((error) => {
        // Handle the error if any
        console.error('Error deleting data:', error);
      });
  };

  return (
    <>
      <div className="table-responsive mt-4" style={{ paddingLeft: '15px', paddingRight: '15px' }}>
        <table className="table table-bordered table-responsive">
          <thead>
            <tr>
              {headers.map((header) => (
                <th key={header} scope="col">
                  {header}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
          {tableData.map((row) => (
  <tr key={row.id}>
   {headers.map((header) => (
  <td key={header} style={{ textAlign: 'center' }}>
    {imgColumns.includes(header) ? (
      <img src={row[header]} alt={`Image ${row.id}`} style={{height:'100px',width:'100%'}} className="img-thumbnail" />
    ) : header === 'incomep' ? (
      <a href={row[header]} target="_blank" rel="noopener noreferrer">PDF Link</a>
    ) : header === 'Final Decision' ? (
      row.status === 1 ? (
        <span className="text-success">Approved
        
        <br/>

        ApprovedAmmount:-{row.ammount}/-
        <br/>
        Emi:-{row.emi}/-
        <br/>
        Months:-{row.month}
        </span>
      ) : row.status === 2 ? (
        <span className="text-danger">Declined</span>
      ) : (
        <>
          <button className="btn btn-success" onClick={() =>handleApprove(row.id)}>Approve</button>
          <Link className="btn btn-danger" style={{marginTop:'5px'}} onClick={() => handleDecline(row.id)}>Decline</Link>
        </>
      )
    ) : header === 'Action' ? (
      <Link className="btn btn-danger" onClick={() => handleDelete(row.id)}>Delete</Link>
    ) : (
      row[header.toLowerCase()]
    )}
  </td>
))}
  </tr>
))}

{showApproveModal && (
  <div className="modal fade show" style={{ display: 'block' }}>
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Approve Loan</h5>
          <button type="button" className="close" onClick={() => setShowApproveModal(false)}>
            <span>&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <form onSubmit={handleApproveFormSubmit}>
            <div className="form-group">
              <label htmlFor="approvedAmount">Approved Amount</label>
              <input
                type="number"
                className="form-control"
                id="approvedAmount"
                value={approvedAmount}
                onChange={(e) => setApprovedAmount(e.target.value)}
              />
            </div>
          
            <div className="form-group">
              <label htmlFor="approvedAmount">Emi Per Month</label>
              <input
                type="number"
                className="form-control"
                id="approvedAmount"
                value={Emi}
                onChange={(e) => setEmi(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="approvedAmount">Loan Tenure Month</label>
              <input
                type="number"
                className="form-control"
                id="approvedAmount"
                value={Month}
                onChange={(e) => setMonth(e.target.value)}
              />
            </div>
            <button type="submit" className="btn btn-success">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
)}


          </tbody>
        </table>
      </div>
    </>
  );
};

export default Sildertabel;
