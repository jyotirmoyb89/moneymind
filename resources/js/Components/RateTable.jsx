import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';


const RateTable = () => {

  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    // Fetch table data from the API using Axios
    axios.get('getdataall/homesettings') // Replace with your actual API endpoint URL
      .then((response) => {
        setTableData(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  }, []);




  return (

    <div className="rate-table" style={{marginTop:'15px', marginBottom:'15px'}}>
      <div className="container">
        <div   className="row">
      {tableData.map((row, index) => (
          <div key={index} className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
            <div className="rate-counter-block">
              <div className="icon rate-icon">
                <img src={row.img} alt="Borrow - Loan Company Website Template" className="icon-svg-1x" />
              </div>
              <div className="rate-box">
                <h1 className="loan-rate">{row.per}%</h1>
                <small className="rate-title">{row.loanname}</small>
              </div>
            </div>
          </div>
          ))}
         
          
        </div>

      </div>
    </div>
  );
};

export default RateTable;
