import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';
const LoanProducts = () => {

  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    // Fetch table data from the API using Axios
    axios.get('getdataall/homesettings') // Replace with your actual API endpoint URL
      .then((response) => {
        setTableData(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        console.error('Error fetching table data:', error);
      });
  }, []);



  return (
    <div className="section-space80">
      <div className="container">
        <div className="row">
          <div className="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
            <div className="mb60 text-center section-title">
              <h1>Find Loan Products We Offer</h1>
              <p>
                We will match you with a loan program that meets your financial need. In short term liquidity, by striving to make funds available to them{' '}
                <strong>within 24 hours of application.</strong>
              </p>
            </div>
          </div>
        </div>
        <div className="row">
        {tableData.map((row, index) => (
          <div key={index} className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
            <div className="bg-white pinside40 service-block outline mb30">
              <div className="icon mb40">
                <img src={row.img} alt="Borrow - Loan Company Website Template" className="icon-svg-2x" />
              </div>
              <h2>
                <p href="#" className="title">
                {row.loanname}
                </p>
              </h2>
              <p>{row.des}</p>
              <Link href="apply" class="btn btn-default">Apply Now</Link>
            </div>
          </div>
          ))}
          {/* <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
            <div className="bg-white pinside40 service-block outline mb30">
              <div className="icon mb40">
                <img src="/assets/images/mortgage.svg" alt="Borrow - Loan Company Website Template" className="icon-svg-2x" />
              </div>
              <h2>
                <Link href="#" className="title">
                  Home Loan
                </Link>
              </h2>
              <p>Sed ut perspiciatis unde omnis rror sit voluptatem accusan tium dolo remque laudantium, totam rem aperiam, eaque ipsa</p>
              <Link href="apply" class="btn btn-default">Apply Now</Link>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
            <div className="bg-white pinside40 service-block outline mb30">
              <div className="icon mb40">
                <img src="/assets/images/mortgage.svg" alt="Borrow - Loan Company Website Template" className="icon-svg-2x" />
              </div>
              <h2>
                <Link href="#" className="title">
                Loan Against Propert
                </Link>
              </h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elmodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>
              <Link href="apply" class="btn btn-default">Apply Now</Link>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
            <div className="bg-white pinside40 service-block outline mb30">
              <div className="icon mb40">
                <img src="/assets/images/loan.svg" alt="Borrow - Loan Company Website Template" className="icon-svg-2x" />
              </div>
              <h2>
                <Link href="#" className="title">
                  Business Loan
                </Link>
              </h2>
              <p>Lorem ipsum dolor sit nean commodo ligula eget dolor simple dummyum sociis natoque.amet, consectetuer adipiscing elit.</p>
              <Link href="apply" class="btn btn-default">Apply Now</Link>
            </div>
          </div>
           */}
        </div>
      </div>
    </div>
  );
};

export default LoanProducts;
