import React from 'react';
import { Link, Head } from '@inertiajs/react';

const Dashboarsidebar = ({ auth }) => {
  return (
    <>
      <nav className="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style={{ background: 'linear-gradient(gray, black)' }}>
        <div className="container-fluid d-flex flex-column p-0">
          <Link className="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="dashboard">
            <div className="sidebar-brand-icon rotate-n-15">
              <i className="fas fa-laugh-wink"></i>
            </div>
            <div className="sidebar-brand-text mx-3">
            <img src="/assets/images/logo-black.svg" alt="Borrow - Loan Company Website Template" style={{width:'200px'}} />
            </div>
          </Link>
          <hr className="sidebar-divider my-0" />
          <ul className="navbar-nav text-light" id="accordionSidebar">
            <li className="nav-item">
              <a className="nav-link active" href="/">
                <i className="fas fa-window-maximize"></i>
                <span>Go To Site</span>
              </a>
            </li>
            
            {auth.user.type === 'Admin' && (
  <li className="nav-item">
    <Link className="nav-link active" href="dashboard">
      <i className="fas fa-window-maximize"></i>
      <span>HOME</span>
    </Link>
  </li>
)}
            
            {auth.user.type === 'user' && (
  <li className="nav-item">
    <Link className="nav-link active" href="UserDashboard">
      <i className="fas fa-window-maximize"></i>
      <span>HOME</span>
    </Link>
  </li>
)}




          
          

            {auth.user.type !== 'user' && (
              <>

                <li className="nav-item">
                  <a className="nav-link collapsed"  data-toggle="collapse" data-target="#collapseSettings" aria-expanded="true" aria-controls="collapseSettings">
                   
                    <span>Settings</span>
                  </a>
                  <div id="collapseSettings" className="collapse" aria-labelledby="headingSettings" data-parent="#accordionSidebar" style={{marginLeft:'2px'}}>
                    <div className="bg-white py-2 collapse-inner rounded">
                      <h6 className="collapse-header"> Pages Settings :</h6>
                      <Link className="collapse-item" href="slider">Slider Settings</Link>
                      <Link className="collapse-item" href="lendersettings">Lender Settings</Link>
                      <Link className="collapse-item" href="homesettings">Home Settings</Link>
                      {/* <Link className="collapse-item" to="#">About Settings</Link>
                      <Link className="collapse-item" to="#">FAQ Settings</Link> */}
                      <Link className="collapse-item" href="loanpagesettings">LOAN Settings</Link>
                     
                      
                    </div>
                  </div>
                </li>



                <li className="nav-item">
                  <Link className="nav-link active" href="leads">
                    <i className="fas fa-window-maximize"></i>
                    <span>View Leads</span>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link active" href="loanenquary">
                    <i className="fas fa-window-maximize"></i>
                    <span>View Loan Enquary</span>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link active" href="userqustion">
                    <i className="fas fa-window-maximize"></i>
                    <span>View Question</span>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
      </nav>
    </>
  );
};

export default Dashboarsidebar;
