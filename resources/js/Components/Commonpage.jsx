import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, Head } from '@inertiajs/react';
const Commonpage = (props) => {


    const [tableData, setTableData] = useState([]);

    useEffect(() => {
      // Fetch table data from the API using Axios
      axios.post('lonetype', {
        name: props.name
       
      })
      .then((response) => {
        // Handle the success response here (e.g., close the modal, update table)
        
        setTableData(response.data);
        console.log(response.data);
        // Perform any other necessary actions, such as updating the table data
      })
      .catch((error) => {
        // Handle the error if any
        console.error('Error updating loan:', error);
      });
    }, []);


  return (
   <>
   <div className=" ">
       
       <div className="container">
           <div className="row">
               <div className="col-xl-12 col-md-12 col-lg-12 col-sm-12 col-12">

               {tableData.map((row, index) => (
                   <div key={index} className="wrapper-content bg-white">
                       <div className="section-scroll pinside60" id="section-about">
                           <h1>About {props.name}</h1>
                           <p className="lead">{row.des} </p>
                           <div className="row">
                                <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                   <p>{row.des1}</p>
                               </div>
                               <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                   <p>{row.des2}</p>
                               </div>
                           </div>
                           <hr/>
                           <p>{row.des3} </p>
                           <Link href="apply" className="btn btn-default">Apply for loan</Link> </div>
                       <div className="section-scroll" id="section-typeloan">
                           <div className="bg-light pinside60">
                               <div className="row">
                                  <div className="col-xl-12 col-md-12 col-lg-12 col-sm-12 col-12">
                                       <div className="section-title mb60">
                                           <h2>Get financing for whatever you need now</h2>
                                           <p>Achieve all your goals and aspirations; with the right kind of help, exactly when you need it.</p>
                                       </div>
                                   </div>
                               </div>
                               <div className="row">
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-blurb mb60">
                                           <h3>{row.des4}</h3>
                                           <p>{row.des5}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-blurb mb60">
                                           <h3>{row.des6}</h3>
                                           <p>{row.des7}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-blurb mb60">
                                           <h3>{row.des8}</h3>
                                           <p>{row.des9}</p>
                                       </div>
                                   </div>
                                  <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-blurb mb60">
                                           <h3>{row.des10}</h3>
                                           <p>{row.des11}</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       
                       <div className="section-scroll" id="section-feature">
                           <div className="pinside60">
                               <div className="row">
                                  <div className="col-xl-12 col-md-12 col-lg-12 col-sm-12 col-12">
                                       <div className="section-title mb60">
                                           <h2>Features of Home Loan</h2>
                                           <p> All loans are not created equal, personal loan has become a great option for people to use.</p>
                                       </div>
                                   </div>
                               </div>
                               <div className="row">
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-icon mb30">
                                           <div className="icon mb20"><i className="icon-employee icon-default icon-2x"></i></div>
                                           <h3>{row.des12}</h3>
                                           <p>{row.des13}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-icon mb30">
                                           <div className="icon mb20"><i className="icon-light-bulb-1 icon-default icon-2x"></i></div>
                                           <h3>{row.des14}</h3>
                                           <p>{row.des15}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-icon mb30">
                                           <div className="icon mb20"><i className="icon-target icon-default icon-2x"></i></div>
                                           <h3>{row.des16}</h3>
                                           <p>{row.des17}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-6 col-md-6 col-lg-6 col-sm-12 col-12">
                                       <div className="feature-icon mb30">
                                           <div className="icon mb20"><i className="icon-dollar-coins icon-default icon-2x"></i></div>
                                           <h3>{row.des18}</h3>
                                           <p>{row.des19}</p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div className="section-scroll" id="section-eleigiblity">
                           <div className=" bg-light pinside60">
                               <div className="row">
                                   <div className="col-xl-12 col-md-12 col-lg-12 col-sm-12 col-12">
                                       <div className="section-title mb60">
                                           <h2>{row.des20}</h2>
                                           <p> {row.des21}</p>
                                       </div>
                                   </div>
                               </div>
                               <div className="row">
                                   <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                       <div className="feature-blurb mb30">
                                           <h3>{row.des22}</h3>
                                           <p>{row.des23}</p>
                                       </div>
                                   </div>
                                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                       <div className="feature-blurb mb30">
                                           <h3>{row.des24}</h3>
                                           <p>{row.des25}</p>
                                       </div>
                                   </div>
                                   <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                       <div className="feature-blurb mb30">
                                           <h3>{row.des26}</h3>
                                           <p>{row.des27}</p>
                                       </div>
                                   </div>
                               </div>
                               <div className="row">
                                   <div className="offset-xl-3 col-xl-6 offset-lg-3 col-lg-6 offset-md-3 col-md-6  col-sm-12 col-12"> </div>
                               </div>
                           </div>
                       </div>
                       
                    
                       <div className="widget-share">
                           <ul className="listnone">
                               <li><a href="#" className="btn-share btn-facebook" title="Share on Facebook"><i className="fa fa-facebook"></i></a></li>
                               <li><a href="#" className="btn-share btn-twitter" title="Share on Twitter"><i className="fa fa-twitter"></i></a></li>
                               <li>
                                   <a href="#" className="btn-share btn-google" title="Share on Google"> <i className="fa fa-google"></i></a>
                               </li>
                               <li><a href="#" className="btn-share btn-linkedin" title="Share on Linkedin"><i className="fa fa-linkedin"></i></a></li>
                           </ul>
                       </div>
                   </div>
               ))}


               </div>
           </div>
       </div>
   </div>
   
   </>
  )
}

export default Commonpage