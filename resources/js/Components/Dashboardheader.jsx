import React from 'react'
import { Link, Head } from '@inertiajs/react';
const Dashboardheader = ({ auth }) => {
  return (
   <>
   
   <nav className="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
            <div className="container-fluid">
             
              <ul className="navbar-nav flex-nowrap ms-auto">
               
                <li className="nav-item dropdown no-arrow mx-1">
                  <a className="nav-link dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" href="#"></a>
                  <div className="dropdown-menu dropdown-menu-end dropdown-list animated--grow-in">
                    <h6 className="dropdown-header">Alerts Center</h6>
                    <a className="dropdown-item d-flex align-items-center" href="#">
                      <div className="me-3">
                        <div className="bg-primary icon-circle">
                          <i className="fas fa-file-alt text-white"></i>
                        </div>
                      </div>
                      <div>
                        <span className="small text-gray-500">December 12, 2019</span>
                        <p>A new monthly report is ready to download!</p>
                      </div>
                    </a>
                    
                  </div>
                </li>
                
                <li className="nav-item dropdown no-arrow">
                  <a className="nav-link dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" href="#">
                    <span className="d-none d-lg-inline me-2 text-gray-600 small">Welcome {auth.user.name}</span>
                    <img className="border rounded-circle img-profile" src="https://img.freepik.com/free-icon/user_318-159711.jpg" alt="Profile" />
                  </a>
                  <div className="dropdown-menu dropdown-menu-end animated--grow-in">
                    <Link className="dropdown-item" href="profile">
                     
                      Profile
                    </Link>
                   
                    <div className="dropdown-divider"></div>
                    <Link className="dropdown-item" href={route('logout')} method="post">
                      
                      Logout
                    </Link>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
   
   
   
   </>
  )
}

export default Dashboardheader