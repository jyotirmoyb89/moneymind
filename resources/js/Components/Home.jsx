import React from 'react'
import Slider from './Slider'
import RateTable from './RateTable'
import { Link, Head } from '@inertiajs/react';
import LoanProducts from './LoanProducts';
import Call from './Call';
const Home = () => {
  return (
    <>
    <Slider/>

    <RateTable/>


<LoanProducts/>

    <div className="bg-white section-space80">
        <div className="container">
            <div className="row">
                 <div className="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div className="mb100 text-center section-title">
                        
                        <h1>Fast &amp; Easy Application Process.</h1>
                        <p>Suspendisse aliquet varius nunc atcibus lacus sit amet coed portaeri sque mami luctus viveed congue lobortis faucibus.</p>
                    </div>
                    
                </div>
            </div>
            <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div className="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div className="circle"><span className="number">1</span></div>
                        <h3 className="number-title">Choose Loan Amount</h3>
                        <p>Suspendisse accumsan imperdue ligula dignissim sit amet vestibulum in mollis etfelis.</p>
                    </div>
                </div>
               <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div className="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div className="circle"><span className="number">2</span></div>
                        <h3 className="number-title">Approved Your Loan</h3>
                        <p>Fusce tempor sstibulum varius sem nec mi luctus viverra edcongue lobortis faucibus.</p>
                    </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div className="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div className="circle"><span className="number">3</span></div>
                        <h3 className="number-title">Get Your Cash</h3>
                        <p>Get your money in minutes simtibulm varius semnec mluctus gue lobortis faucibus.</p>
                    </div>
                </div>
            </div>
            <div className="row">
                
            </div>
        </div>
    </div>

    <div className="section-space80 bg-white">
        <div className="container">
            <div className="row">
               <div className="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div className="mb60 text-center section-title">
                        
                        <h1>We are Here to Help You</h1>
                        <p>Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    
                </div>
            </div>
          

        <Call/>



        </div>
    </div>



    </>
  )
}

export default Home