import React, { useState, useEffect } from 'react';
import { Link, Head } from '@inertiajs/react';
import axios from 'axios';

const Footer = () => {

  const [adminUsers, setAdminUsers] = useState([]);

  useEffect(() => {
    // Fetch the data from the Laravel API
    axios.get('/user')
      .then(response => {
        setAdminUsers(response.data);
      })
      .catch(error => {
        console.error('Error fetching admin users:', error);
      });
  }, []);


  return (
   <>
    <div className="footer section-space100">
        {/* footer */}
        <div className="container">
          <div className="row">
            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
              <div className="footer-logo">
                {/* Footer Logo */}
                <img src="/assets/images/logo-black.svg" alt="Borrow - Loan Company Website Templates"  style={{ filter: 'invert(100%)' }}/> 
              </div>
              {/* /.Footer Logo */}
            </div>
            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                  <h3 className="newsletter-title">Signup Our Newsletter</h3>
                </div>
                <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                  <div className="newsletter-form">
                    {/* Newsletter Form */}
                    <form action="#" method="post">
                      <div className="input-group">
                        <input type="email" className="form-control" id="newsletter" name="newsletter" placeholder="Write E-Mail Address" required />
                        <span className="input-group-btn">
                          <button className="btn btn-default" type="submit">Go!</button>
                        </span> 
                      </div>
                      {/* /input-group */}
                    </form>
                  </div>
                  {/* /.Newsletter Form */}
                </div>
              </div>
              {/* /.col-lg-6 */}
            </div>
          </div>
          <hr className="dark-line" />
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
              <div className="widget-text mt40">
                {/* widget text */}
                <p>Our goal at Borrow Loan Company is to provide access to personal loans and education loan, car loan, home loan at insight competitive interest rates lorem ipsums. We are the loan provider, you can use our loan product.</p>
                <div className="row">
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                    <p className="address-text"><span><i className="icon-placeholder-3 icon-1x"></i> </span>3895 Sycamore Road Arlington, 97812 </p>
                  </div>
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                    
                  {adminUsers.map(item => (
          <p key={item.id} className="call-text">
            <span><i className="icon-phone-call icon-1x"></i></span>
            {item.phone}
          </p>
        ))}
                  </div>
                </div>
              </div>
              {/* /.widget text */}
            </div>
            <div className="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
              <div className="widget-footer mt40">
                {/* widget footer */}
                <ul className="listnone">
                  <li><Link href="/">Home</Link></li>
                  
                  <li><Link href="about">About Us</Link></li>
                  
                  <li><Link href="faq">Faq</Link></li>
                  <li><Link href="contactus">Contact Us</Link></li>
                  <li><Link href="loancalculator">Loan Calculator</Link></li>
                </ul>
              </div>
              {/* /.widget footer */}
            </div>
            <div className="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
              <div className="widget-footer mt40">
                {/* widget footer */}
                <ul className="listnone">
                  <li><Link href="loanagainstproperty">Loan Against Property</Link></li>
                  <li><Link href="personalloan">Personal Loan</Link></li>
                 
                  <li><Link href="businessloan">Business Loan</Link></li>
                  <li><Link href="homeloan">Home Loan</Link></li>
                 
                </ul>
              </div>
              {/* /.widget footer */}
            </div>
            <div className="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
              <div className="widget-social mt40">
                {/* widget footer */}
                <ul className="listnone">
                  <li><Link to="#"><i className="fa fa-facebook"></i>Facebook</Link></li>
                  <li><Link to="#"><i className="fa fa-google-plus"></i>Google Plus</Link></li>
                  <li><Link to="#"><i className="fa fa-twitter"></i>Twitter</Link></li>
                  <li><Link to="#"><i className="fa fa-linkedin"></i>Linked In</Link></li>
                </ul>
              </div>
              {/* /.widget footer */}
            </div>
          </div>
        </div>
      </div>
      <div className="tiny-footer">
        {/* tiny footer */}
        <div className="container">
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
              <p><a target="_blank" href="">Moneyminds</a></p>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-right">
              <p>Terms of use | Privacy Policy</p>
            </div>
          </div>
        </div>
      </div>
   
   </>
  )
}

export default Footer