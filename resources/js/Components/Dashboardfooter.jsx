import React from 'react'

const Dashboardfooter = () => {
  return (
    <>
    
    <footer className="bg-white sticky-footer" style={{marginTop:'30px'}}>
          <div className="container my-auto">
            <div className="text-center my-auto copyright">
              <span>Copyright © Moneyminds 2023</span>
            </div>
          </div>
        </footer>
    
    
    </>
  )
}

export default Dashboardfooter