import{r as l,c,j as s,F as n,a as e}from"./app-070633a4.js";const o=()=>{const[t,i]=l.useState([]);return l.useEffect(()=>{c.get("getdataall/landers").then(a=>{i(a.data),console.log(a.data)}).catch(a=>{console.error("Error fetching table data:",a)})},[]),s(n,{children:[e("div",{className:"section-space140",children:s("div",{className:"container-fluid",children:[e("div",{className:"row",children:e("div",{className:"offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12",children:s("div",{className:"section-title mb60 text-center",children:[e("h1",{children:"Meet our lenders"}),e("p",{children:"Compare 60+ business funding options & check eligibility saving you time/money"})]})})}),e("div",{className:"row",children:e("div",{className:"col-12",children:e("div",{className:"lender-container",style:{display:"flex",overflowX:"hidden",width:"100%"},children:t.map((a,r)=>e("div",{className:"lender-block bg-boxshadow",style:{flex:"0 0 auto",marginRight:"10px",animation:"scrollLenders 20s linear infinite"},children:e("img",{src:a.img,alt:"",style:{height:"50px",width:"150px",objectFit:"cover"}})},r))})})})]})}),e("style",{children:`
        @keyframes scrollLenders {
          0% {
            transform: translateX(0);
          }
          100% {
            transform: translateX(calc(-100% - 10px));
          }
        }
      `})]})},m=o;export{m as L};
