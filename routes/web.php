<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GetdataController;
use App\Http\Controllers\SliderdataController;
use App\Http\Controllers\HomesettingsController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\ApplyloanController;
use App\Http\Controllers\LoanquaryController;
use App\Http\Controllers\ContactsettingsController;
use App\Http\Controllers\LanderController;



use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('homeloan', function(){
return Inertia::render('Studentsloan');
});
Route::get('businessloan', function(){
return Inertia::render('Businessloan');
});
Route::get('personalloan', function(){
return Inertia::render('Personalloan');
});
Route::get('loanagainstproperty', function(){
return Inertia::render('Loanagainstproperty');
});
Route::get('about', function(){
return Inertia::render('Aboutus');
});
Route::get('faq', function(){
return Inertia::render('Faq');
});
Route::get('loancalculator', function(){
return Inertia::render('Loancalculator');
});
Route::get('contactus', function(){
return Inertia::render('Contacus');
});

Route::get('/user', [GetdataController::class, 'index']);

Route::get('/hmst', [GetdataController::class, 'home']);

Route::post('loanquary', [LoanquaryController::class, 'store'])->name('loanquary');
Route::post('quary', [ContactsettingsController::class, 'store'])->name('quary');

Route::get('getdataall/{data}', [GetdataController::class, 'all']);
Route::post('lonetype', [GetdataController::class, 'store'])->name('lonetype');

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/dashboard', function () {
    if (auth()->user()->type === 'Admin') {
        return Inertia::render('Dashboard'); // Replace 'AdminDashboard' with the actual view for admin
    } else {
        return Inertia::render('UserDashboard'); // Replace 'UserDashboard' with the actual view for non-admin users
    }
})->middleware(['auth', 'verified'])->name('dashboard');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('apply', function(){return Inertia::render('Apply');});

    Route::get('Dashboard', function(){
        return Inertia::render('Dashboard');
        });


    Route::get('UserDashboard', function(){
        return Inertia::render('UserDashboard');
        });


    Route::get('slider', function(){
        return Inertia::render('Dashslider');
        });
   
    
        Route::post('slideradd', [SliderdataController::class, 'edit'])->name('addslider');
        Route::get('sliderdet', [SliderdataController::class, 'index']);

        Route::get('sliderdett/{id}', [SliderdataController::class, 'destroy']);


        Route::get('homesettings', function(){
            return Inertia::render('Homesettings');
            });
        Route::get('loanpagesettings', function(){
            return Inertia::render('Loanpage');
            });


            

            Route::post('homesettingsup', [HomesettingsController::class, 'update'])->name('homesettingsupdate');

            Route::get('/loanset', [GetdataController::class, 'loansettings']);

            Route::post('loansettingsup', [LoanController::class, 'update'])->name('loansettingspageupdate');
            Route::post('applynowloan', [ApplyloanController::class, 'store'])->name('applynowloan');

            Route::get('leads', function(){
                return Inertia::render('Leadspage');
                });
            Route::get('loanenquary', function(){
                return Inertia::render('Loanenquary');
                });
            Route::get('userqustion', function(){
                return Inertia::render('Dashqustion');
                });

                Route::get('getall/{data}', [GetdataController::class, 'all']);

                Route::post('approve', [ApplyloanController::class, 'edit'])->name('approval');

                Route::get('decline/{id}', [ApplyloanController::class, 'update']);
                Route::get('del/{id}', [ApplyloanController::class, 'destroy']);


                Route::get('delloanquary/{id}', [LoanquaryController::class, 'destroy']);
Route::get('delquary/{id}', [ContactsettingsController::class, 'destroy']);

Route::get('getusepanelloan', [GetdataController::class, 'userloan']);


Route::get('lendersettings', function(){
    return Inertia::render('Lendersettings');
    });
    
    

    Route::post('lenderpost', [LanderController::class, 'store'])->name('lenderpost');
    Route::get('dellenderpost/{id}', [LanderController::class, 'destroy']);          
});

require __DIR__.'/auth.php';
