<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string('loanname');
            $table->string('des1');
            $table->string('des2');
            $table->string('des3');
            $table->string('des4');
            $table->string('des5');
            $table->string('des6');
            $table->string('des7');
            $table->string('des8');
            $table->string('des9');
            $table->string('des10');
            $table->string('des11');
            $table->string('des12');
            $table->string('des13');
            $table->string('des14');
            $table->string('des15');
            $table->string('des16');
            $table->string('des17');
            $table->string('des18');
            $table->string('des19');
            $table->string('des20');
            $table->string('des21');
            $table->string('des22');
            $table->string('des23');
            $table->string('des24');
            $table->string('des25');
            $table->string('des26');
            $table->string('des27');
            $table->string('des28');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('loans');
    }
};
