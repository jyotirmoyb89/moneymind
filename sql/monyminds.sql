-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2023 at 07:42 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monyminds`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutsettings`
--

CREATE TABLE `aboutsettings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `whoweare` varchar(255) NOT NULL,
  `whoweoffer` varchar(255) NOT NULL,
  `ourvisionandmission` varchar(255) NOT NULL,
  `whyapplywithus` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aboutsettings`
--

INSERT INTO `aboutsettings` (`id`, `whoweare`, `whoweoffer`, `ourvisionandmission`, `whyapplywithus`, `created_at`, `updated_at`) VALUES
(1, 'Insight loan advisors is completely independent loan advising service and our directory of lenders gives you all the information lorem ipsums sitamets.', 'Our loan sanction is one of the quicke with eas documentation and doorstep lorem ipsum serviceullam dolor sitisi.', 'Our goal at Insight Loan Advisors is to provide access to personal loans, car loan, at insight competitive interest raa timely mannerlorem ipsums deconse resonescon.', 'Quisque in augunean suscipit ipsum nibh sit amet venerem', '2023-08-07 18:23:56', '2023-08-07 18:23:56');

-- --------------------------------------------------------

--
-- Table structure for table `applyloans`
--

CREATE TABLE `applyloans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loanid` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phn` varchar(255) NOT NULL,
  `add` varchar(255) NOT NULL,
  `incometype` varchar(255) NOT NULL,
  `loan` varchar(2555) NOT NULL,
  `monthlyincome` varchar(255) NOT NULL,
  `aadhar` varchar(255) NOT NULL,
  `pan` varchar(255) NOT NULL,
  `incomep` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `ammount` decimal(10,0) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `emi` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applyloans`
--

INSERT INTO `applyloans` (`id`, `loanid`, `name`, `email`, `phn`, `add`, `incometype`, `loan`, `monthlyincome`, `aadhar`, `pan`, `incomep`, `status`, `ammount`, `month`, `emi`, `created_at`, `updated_at`) VALUES
(1, 'LOAN3127026QSO', 'Arunavo', 'arunava.misc@gmail.com', '9876543120', 'tfsdetsdgsedrgsegsdgsgsgsg', 'selfemployyed', 'business', '4534535', 'http://127.0.0.1:8000/storage/uploads/avatar2.jpeg', 'http://127.0.0.1:8000/storage/uploads/avatar4.jpeg', 'http://127.0.0.1:8000/storage/uploads/g.pdf', 1, 500000, 36, 5000, '2023-08-10 05:46:42', '2023-08-10 07:23:02'),
(2, 'LOAN8003774JBP', 'Arunavo', 'arunava.misc@gmail.com', '9876543210', 'tfsdetsdgsedrgsegsdgsgsgsg', 'salaried', 'home', '45000', 'http://127.0.0.1:8000/storage/uploads/avatar2.jpeg', 'http://127.0.0.1:8000/storage/uploads/avatar3.jpeg', 'http://127.0.0.1:8000/storage/uploads/g.pdf', 2, NULL, NULL, NULL, '2023-08-10 06:00:15', '2023-08-10 07:25:38');

-- --------------------------------------------------------

--
-- Table structure for table `contactsettings`
--

CREATE TABLE `contactsettings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qustiontype` varchar(255) NOT NULL,
  `qustion` varchar(255) NOT NULL,
  `ans` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `homesettings`
--

CREATE TABLE `homesettings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) NOT NULL,
  `loanname` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  `per` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homesettings`
--

INSERT INTO `homesettings` (`id`, `img`, `loanname`, `des`, `per`, `created_at`, `updated_at`) VALUES
(1, '/assets/images/mortgage.svg', 'Home Loan', 'To make your home loan journey a smooth sail, in this article we will help you to know eligibility criteria, rates of interest, process, necessary documents, comparison and transfer for lowest rates.', '3.74', '2023-08-07 17:49:25', '2023-08-07 17:49:25'),
(2, '/assets/images/loan.svg', 'Personal Loan', 'Sed ut perspiciatis unde omnis rror sit voluptatem accusan tium dolo remque laudantium, totam rem aperiam, eaque ipsa', '3.74', '2023-08-07 18:13:55', '2023-08-07 18:13:59'),
(3, '/assets/images/mortgage.svg', 'Loan Against Propert', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elmodo ligula eget dolor. Aenean massa. Cum sociis natoque', '10.71', '2023-08-07 18:14:15', '2023-08-07 18:14:15'),
(4, '/assets/images/loan.svg', 'Business Loan', 'Lorem ipsum dolor sit nean commodo ligula eget dolor simple dummyum sociis natoque.amet, consectetuer adipiscing elit.', '9.00', '2023-08-07 18:15:52', '2023-08-07 18:15:55');

-- --------------------------------------------------------

--
-- Table structure for table `landers`
--

CREATE TABLE `landers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landers`
--

INSERT INTO `landers` (`id`, `img`, `created_at`, `updated_at`) VALUES
(1, 'http://127.0.0.1:8000/storage/uploads/Kotak_Mahindra_Bank_logo.png', '2023-08-10 09:30:22', '2023-08-10 09:30:22'),
(5, 'http://127.0.0.1:8000/storage/uploads/download.png', '2023-08-10 09:43:55', '2023-08-10 09:43:55'),
(6, 'http://127.0.0.1:8000/storage/uploads/benefits_of_business_loans.webp', '2023-08-10 10:33:19', '2023-08-10 10:33:19'),
(7, 'http://127.0.0.1:8000/storage/uploads/personal-loan.png', '2023-08-10 10:33:26', '2023-08-10 10:33:26'),
(8, 'http://127.0.0.1:8000/storage/uploads/Home-Loan.webp', '2023-08-10 10:33:34', '2023-08-10 10:33:34'),
(9, 'http://127.0.0.1:8000/storage/uploads/1b7e6724e01a653e64cea902828c65ac.png', '2023-08-10 10:36:15', '2023-08-10 10:36:15'),
(10, 'http://127.0.0.1:8000/storage/uploads/sbi.jpg', '2023-08-10 10:36:23', '2023-08-10 10:36:23'),
(11, 'http://127.0.0.1:8000/storage/uploads/idfc-first-bank-logo.png', '2023-08-10 10:36:34', '2023-08-10 10:36:34'),
(12, 'http://127.0.0.1:8000/storage/uploads/HDFC-Bank-logo.jpg', '2023-08-10 10:36:43', '2023-08-10 10:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `loanquaries`
--

CREATE TABLE `loanquaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loantype` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phn` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lone`
--

CREATE TABLE `lone` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loanname` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  `des1` varchar(255) DEFAULT NULL,
  `des2` varchar(255) DEFAULT NULL,
  `des3` varchar(255) DEFAULT NULL,
  `des4` varchar(255) DEFAULT NULL,
  `des5` varchar(255) DEFAULT NULL,
  `des6` varchar(255) DEFAULT NULL,
  `des7` varchar(255) DEFAULT NULL,
  `des8` varchar(255) DEFAULT NULL,
  `des9` varchar(255) DEFAULT NULL,
  `des10` varchar(255) DEFAULT NULL,
  `des11` varchar(255) DEFAULT NULL,
  `des12` varchar(255) DEFAULT NULL,
  `des13` varchar(255) DEFAULT NULL,
  `des14` varchar(255) DEFAULT NULL,
  `des15` varchar(255) DEFAULT NULL,
  `des16` varchar(255) DEFAULT NULL,
  `des17` varchar(255) DEFAULT NULL,
  `des18` varchar(255) DEFAULT NULL,
  `des19` varchar(255) DEFAULT NULL,
  `des20` varchar(255) DEFAULT NULL,
  `des21` varchar(255) DEFAULT NULL,
  `des22` varchar(255) DEFAULT NULL,
  `des23` varchar(255) DEFAULT NULL,
  `des24` varchar(255) DEFAULT NULL,
  `des25` varchar(255) DEFAULT NULL,
  `des26` varchar(255) DEFAULT NULL,
  `des27` varchar(255) DEFAULT NULL,
  `per` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lone`
--

INSERT INTO `lone` (`id`, `loanname`, `des`, `des1`, `des2`, `des3`, `des4`, `des5`, `des6`, `des7`, `des8`, `des9`, `des10`, `des11`, `des12`, `des13`, `des14`, `des15`, `des16`, `des17`, `des18`, `des19`, `des20`, `des21`, `des22`, `des23`, `des24`, `des25`, `des26`, `des27`, `per`, `created_at`, `updated_at`) VALUES
(1, 'Home Loan', 'To make your home loan journey a smooth sail, in this article we will help you to know eligibility criteria, rates of interest, process, necessary documents, comparison and transfer for lowest rates.', 'Maecenas in ultricies sem. Nunc eget orci mi. Sed porttitor lacus quis scelerisque dignissim. Nullam bibendum lputatesAliquam non mageselislacerat sapien dolor et dui.', 'Sed sit amet volutpat erat. Lorem ipsum dolor sit amet lorem consectetur adipiscing elit. Nam massa ipsum, mollis et sit amet ullamcorque duraesent nec vehicula dolor.', 'Phasellus tellus nunc, sollicitudin quist amet it simple nequeuisque lacus mi tesimly diummy cintenbt mpus nec purus vitae tempor placerat leo.', 'New Homes', 'New Home Loan at basic interest rates from Borrow-Loan Company. You can apply online and check your eligibility and easy EMI. Fast Approval for your new home loan.', 'Home Conversion', 'A home conversion loan is a scheme for those who have already taken a housing loan. This loan follow some rules and regulations.It is a part of loan.', 'Land Purchase', 'Borrow Loan offers home loan for land purchase to make your dream home. You can compare home loan rates with our compare loan table. Apply online for Home Loan', 'Home Renovation', 'Get instant approval for renovation your home. Borrow introduce home improvement loan. It is with basic rate and flexible EMI repayment.For more detail you can check our loan products.', 'Salaried & Self Employed', 'Our loan sanction is one of the quicke with eas documentation and doorstep service ullam nisnisi.', 'Loan For Agriculturists', 'All charges are communicated up front in writing along with the loan quotation uisque euismdolor at tincidunt.', 'Home Loans For NRIs', 'Our loan rates and charges are very attractive lorem ipsums sitamet uerse ipsurabitulectus mattis vitae.', 'No Morgage', 'Nulla vitae tempus nibh. Vestibulum condimentum neque at interdrem ac fristibulum porttitor euismod urna.', 'Home Loan Eligibility', 'Any salaried, self-employed or professional Public and Privat companies, Government sector employees including Public Sector is eligible for a personal loan.', 'Age', '60 yearsl', 'Income', 'Minimum Net Monthly Income: Rs 15,000', 'Credit Rating', 'Applicant should have the bank specified credit score.', '3.74', '2023-08-07 17:49:25', '2023-08-07 17:49:25'),
(2, 'Personal Loan', 'To make your home loan journey a smooth sail, in this article we will help you to know eligibility criteria, rates of interest, process, necessary documents, comparison and transfer for lowest rates.', 'Maecenas in ultricies sem. Nunc eget orci mi. Sed porttitor lacus quis scelerisque dignissim. Nullam bibendum lputatesAliquam non mageselislacerat sapien dolor et dui.', 'Sed sit amet volutpat erat. Lorem ipsum dolor sit amet lorem consectetur adipiscing elit. Nam massa ipsum, mollis et sit amet ullamcorque duraesent nec vehicula dolor.', 'Phasellus tellus nunc, sollicitudin quist amet it simple nequeuisque lacus mi tesimly diummy cintenbt mpus nec purus vitae tempor placerat leo.', 'New HomeX', 'New Home Loan at basic interest rates from Borrow-Loan Company. You can apply online and check your eligibility and easy EMI. Fast Approval for your new home loan.', 'Home Conversion', 'A home conversion loan is a scheme for those who have already taken a housing loan. This loan follow some rules and regulations.It is a part of loan.', 'Land Purchase', 'Borrow Loan offers home loan for land purchase to make your dream home. You can compare home loan rates with our compare loan table. Apply online for Home Loan', 'Home Renovation', 'Get instant approval for renovation your home. Borrow introduce home improvement loan. It is with basic rate and flexible EMI repayment.For more detail you can check our loan products.', 'Salaried & Self Employed', 'Our loan sanction is one of the quicke with eas documentation and doorstep service ullam nisnisi.', 'Loan For Agriculturists', 'All charges are communicated up front in writing along with the loan quotation uisque euismdolor at tincidunt.', 'Home Loans For NRIs', 'Our loan rates and charges are very attractive lorem ipsums sitamet uerse ipsurabitulectus mattis vitae.', 'No Morgage', 'Nulla vitae tempus nibh. Vestibulum condimentum neque at interdrem ac fristibulum porttitor euismod urna.', 'Home Loan Eligibility', 'Any salaried, self-employed or professional Public and Privat companies, Government sector employees including Public Sector is eligible for a personal loan.', 'Age', 'Maximum age of applicant at loan maturity: 60 years', 'Income', 'Minimum Net Monthly Income: Rs 15,000', 'Credit Rating', 'Applicant should have the bank specified credit score.', '3.74', '2023-08-07 17:49:25', '2023-08-07 17:49:25'),
(3, 'Loan Against Propert', 'To make your home loan journey a smooth sail, in this article we will help you to know eligibility criteria, rates of interest, process, necessary documents, comparison and transfer for lowest rates.', 'Maecenas in ultricies sem. Nunc eget orci mi. Sed porttitor lacus quis scelerisque dignissim. Nullam bibendum lputatesAliquam non mageselislacerat sapien dolor et dui.', 'Sed sit amet volutpat erat. Lorem ipsum dolor sit amet lorem consectetur adipiscing elit. Nam massa ipsum, mollis et sit amet ullamcorque duraesent nec vehicula dolor.', 'Phasellus tellus nunc, sollicitudin quist amet it simple nequeuisque lacus mi tesimly diummy cintenbt mpus nec purus vitae tempor placerat leo.', 'New HomeC', 'New Home Loan at basic interest rates from Borrow-Loan Company. You can apply online and check your eligibility and easy EMI. Fast Approval for your new home loan.', 'Home Conversion', 'A home conversion loan is a scheme for those who have already taken a housing loan. This loan follow some rules and regulations.It is a part of loan.', 'Land Purchase', 'Borrow Loan offers home loan for land purchase to make your dream home. You can compare home loan rates with our compare loan table. Apply online for Home Loan', 'Home Renovation', 'Get instant approval for renovation your home. Borrow introduce home improvement loan. It is with basic rate and flexible EMI repayment.For more detail you can check our loan products.', 'Salaried & Self Employed', 'Our loan sanction is one of the quicke with eas documentation and doorstep service ullam nisnisi.', 'Loan For Agriculturists', 'All charges are communicated up front in writing along with the loan quotation uisque euismdolor at tincidunt.', 'Home Loans For NRIs', 'Our loan rates and charges are very attractive lorem ipsums sitamet uerse ipsurabitulectus mattis vitae.', 'No Morgage', 'Nulla vitae tempus nibh. Vestibulum condimentum neque at interdrem ac fristibulum porttitor euismod urna.', 'Home Loan Eligibility', 'Any salaried, self-employed or professional Public and Privat companies, Government sector employees including Public Sector is eligible for a personal loan.', 'Age', 'Maximum age of applicant at loan maturity: 60 years', 'Income', 'Minimum Net Monthly Income: Rs 15,000', 'Credit Rating', 'Applicant should have the bank specified credit score.', '3.74', '2023-08-07 17:49:25', '2023-08-07 17:49:25'),
(4, 'Business Loan', 'To make your home loan journey a smooth sail, in this article we will help you to know eligibility criteria, rates of interest, process, necessary documents, comparison and transfer for lowest rates.', 'Maecenas in ultricies sem. Nunc eget orci mi. Sed porttitor lacus quis scelerisque dignissim. Nullam bibendum lputatesAliquam non mageselislacerat sapien dolor et dui.', 'Sed sit amet volutpat erat. Lorem ipsum dolor sit amet lorem consectetur adipiscing elit. Nam massa ipsum, mollis et sit amet ullamcorque duraesent nec vehicula dolor.', 'Phasellus tellus nunc, sollicitudin quist amet it simple nequeuisque lacus mi tesimly diummy cintenbt mpus nec purus vitae tempor placerat leo.', 'New Home', 'New Home Loan at basic interest rates from Borrow-Loan Company. You can apply online and check your eligibility and easy EMI. Fast Approval for your new home loan.', 'Home Conversion', 'A home conversion loan is a scheme for those who have already taken a housing loan. This loan follow some rules and regulations.It is a part of loan.', 'Land Purchase', 'Borrow Loan offers home loan for land purchase to make your dream home. You can compare home loan rates with our compare loan table. Apply online for Home Loan', 'Home Renovation', 'Get instant approval for renovation your home. Borrow introduce home improvement loan. It is with basic rate and flexible EMI repayment.For more detail you can check our loan products.', 'Salaried & Self Employed', 'Our loan sanction is one of the quicke with eas documentation and doorstep service ullam nisnisi.', 'Loan For Agriculturists', 'All charges are communicated up front in writing along with the loan quotation uisque euismdolor at tincidunt.', 'Home Loans For NRIs', 'Our loan rates and charges are very attractive lorem ipsums sitamet uerse ipsurabitulectus mattis vitae.', 'No Morgage', 'Nulla vitae tempus nibh. Vestibulum condimentum neque at interdrem ac fristibulum porttitor euismod urna.', 'Home Loan Eligibility', 'Any salaried, self-employed or professional Public and Privat companies, Government sector employees including Public Sector is eligible for a personal loan.', 'Age', 'Maximum age of applicant at loan maturity: 60 years', 'Income', 'Minimum Net Monthly Income: Rs 15,000', 'Credit Rating', 'Applicant should have the bank specified credit score.', '3.74', '2023-08-07 17:49:25', '2023-08-07 17:49:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_08_06_191333_create_sliderdatas_table', 2),
(6, '2023_08_07_160908_create_aboutsettings_table', 3),
(7, '2023_08_07_160949_create_contactsettings_table', 4),
(8, '2023_08_07_160922_create_faqs_table', 5),
(9, '2023_08_07_171836_create_landers_table', 6),
(10, '2023_08_07_160810_create_homesettings_table', 7),
(11, '2023_08_06_135919_create_loans_table', 8),
(12, '2023_08_09_093604_create_applyloans_table', 9),
(13, '2023_08_10_074703_create_loanquaries_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_reset_tokens`
--

INSERT INTO `password_reset_tokens` (`email`, `token`, `created_at`) VALUES
('pritamchakraborty25@gmail.com', '$2y$10$tyS12yIylU30sB.K7HVY4OecEof621oBAm3UNEaJkslEu9tHSpPva', '2023-08-06 04:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliderdatas`
--

CREATE TABLE `sliderdatas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliderdatas`
--

INSERT INTO `sliderdatas` (`id`, `content`, `img`, `created_at`, `updated_at`) VALUES
(3, 'Business Loans', 'http://127.0.0.1:8000/storage/uploads/benefits_of_business_loans.webp', '2023-08-10 09:01:51', '2023-08-10 09:01:51'),
(4, 'Home Loan', 'http://127.0.0.1:8000/storage/uploads/Home-Loan.webp', '2023-08-10 09:44:34', '2023-08-10 09:44:34'),
(5, 'Personal Loan to Suit Your Needs.', 'http://127.0.0.1:8000/storage/uploads/personal-loan.png', '2023-08-10 09:45:19', '2023-08-10 09:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'user',
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `type`, `email`, `phone`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admins', 'Admin', 'admin@gmail.com', '9876543214', NULL, '$2y$10$XKcH8k6NI.X0HaShV8aQ0ecOy7bKOIdgpUyfEAGACihaUrPU6VBxS', 'NwUUuu5CZejlPWiC0vCkPP3wSNY67v5o0lA9C4cmcrmeuiLQV7YxmuWzquxf', '2023-08-05 14:22:57', '2023-08-06 08:21:23'),
(2, 'test', 'user', 'arunava.misc@gmail.com', '9876543210', NULL, '$2y$10$fxtEWiPI9Mbv5qZsJNvo.uiW484xQVBO5crfoJOGcasS6v0nxA36K', NULL, '2023-08-09 10:24:55', '2023-08-10 05:56:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutsettings`
--
ALTER TABLE `aboutsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applyloans`
--
ALTER TABLE `applyloans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactsettings`
--
ALTER TABLE `contactsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homesettings`
--
ALTER TABLE `homesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landers`
--
ALTER TABLE `landers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loanquaries`
--
ALTER TABLE `loanquaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lone`
--
ALTER TABLE `lone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `sliderdatas`
--
ALTER TABLE `sliderdatas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutsettings`
--
ALTER TABLE `aboutsettings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `applyloans`
--
ALTER TABLE `applyloans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contactsettings`
--
ALTER TABLE `contactsettings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homesettings`
--
ALTER TABLE `homesettings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `landers`
--
ALTER TABLE `landers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `loanquaries`
--
ALTER TABLE `loanquaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lone`
--
ALTER TABLE `lone`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliderdatas`
--
ALTER TABLE `sliderdatas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
