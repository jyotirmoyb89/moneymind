<?php

namespace App\Http\Controllers;

use App\Models\lander;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class LanderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,gif,webp|max:2048', // Required image field with specific MIME types and file size limit
           
        ]);

        $path = '';
        
        $currentURL = url()->to('/');

        if ($request->hasFile('photo')) {
            $ffname = $request->file('photo')->getClientOriginalName();
            $fpath = $request->file('photo')->move('storage/uploads', $ffname);
            $path = $currentURL . '/' .'storage/uploads/' . $ffname;
        }

        DB::table('landers')->insert([

            'img'=>$path,

            'created_at' => now(),
            'updated_at' => now()
        ]);

        return response()->json('success');

    }

    /**
     * Display the specified resource.
     */
    public function show(lander $lander)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(lander $lander)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, lander $lander)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('landers')->where('id', $id)->delete();
        return response()->json('success'); 
    }
}
