<?php

namespace App\Http\Controllers;

use App\Models\getdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GetdataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $usr= DB::table('users')->where('type', 'Admin')->get();
       return response()->json($usr);

    }

    public function home()
    {
       $hm= DB::table('homesettings')->get();
       return response()->json($hm);

    }


    public function loansettings()
    {
       $hm= DB::table('lone')->get();
       return response()->json($hm);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function all($data)
    {
        
        $returns=DB::table($data)->get();
        return response()->json($returns);

    }
    public function userloan()
    {
        
        $usr= DB::table('applyloans')->where('email', Auth::user()->email)->get();
        return response()->json($usr);
        

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $usr= DB::table('lone')->where('loanname', $request->name)->get();
        return response()->json($usr);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(getdata $getdata)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, getdata $getdata)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(getdata $getdata)
    {
        //
    }
}
