<?php

namespace App\Http\Controllers;

use App\Models\loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
{
    $id = $request->input('id');
    $data = $request->except('id'); // Remove the 'id' from the data

    try {
        $affectedRows = DB::table('lone')->where('id', $id)->update($data);
    
        if ($affectedRows > 0) {
            return response()->json(['message' => 'Loan updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Loan not found or not updated'], 404);
        }
    } catch (\Exception $e) {
        return response()->json(['message' => 'Error updating loan: ' . $e->getMessage()], 500);
    }
}


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(loan $loan)
    {
        //
    }
}
