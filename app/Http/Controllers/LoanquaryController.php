<?php

namespace App\Http\Controllers;

use App\Models\loanquary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanquaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $data['created_at'] = now();
            $data['updated_at'] = now();
            
            DB::table('loanquaries')->insert($data);
            return response()->json('success');
        } catch (\Exception $e) {
            return response()->json('error', 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(loanquary $loanquary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(loanquary $loanquary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, loanquary $loanquary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('loanquaries')->where('id', $id)->delete();
        return response()->json('success'); 
    }
}
