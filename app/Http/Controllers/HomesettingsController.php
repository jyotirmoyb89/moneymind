<?php

namespace App\Http\Controllers;

use App\Models\homesettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomesettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(homesettings $homesettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(homesettings $homesettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
{
    // Step 1: Fetch the ID from the request data
    $id = $request->input('id');

    // Step 2: Validate and process the incoming request data (you can define your validation rules here)
    $validatedData = $request->validate([
        'loanname' => 'required|string|max:255',
        'des' => 'required|string',
        'per' => 'required|numeric',
    ]);

    // Step 3: Check if the record exists in the database
    $loanData = DB::table('homesettings')->find($id);

    if (!$loanData) {
        return response()->json(['message' => 'Loan not found'], 404);
    }

    // Step 4: Update the record with the new data
    DB::table('homesettings')->where('id', $id)->update($validatedData);

    // Return a response to indicate success
    return response()->json(['message' => 'Loan updated successfully'], 200);
}

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(homesettings $homesettings)
    {
        //
    }
}
