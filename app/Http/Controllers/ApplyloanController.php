<?php

namespace App\Http\Controllers;

use App\Models\applyloan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ApplyloanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $prefix = 'LOAN';
$randomNumber = mt_rand(1000000, 9999999); // Generate a random number between 1000000 and 9999999
$randomString = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 3); // Generate a random 3-character string from the alphabet

$randomID = $prefix . $randomNumber . $randomString;

        $request->validate([
            'aadhar' => 'required|image|mimes:jpeg,png,gif|max:2048', // Required image field with specific MIME types and file size limit
            'pan' => 'required|image|mimes:jpeg,png,gif|max:2048', // Required image field with specific MIME types and file size limit
            'incomep' => 'required|mimes:pdf|max:2048', // Required image field with specific MIME types and file size limit
        ]);

    // $data = $request->except('id','aadhar','pan','incomep'); // Remove the 'id' from the data


    $path1 = '';
    $path2 = '';
    $path3 = '';
    $currentURL = url()->to('/');
        if ($request->hasFile('aadhar')) {
            $ffname = $request->file('aadhar')->getClientOriginalName();
            $fpath = $request->file('aadhar')->move('storage/uploads', $ffname);
            $path1 = $currentURL . '/' .'storage/uploads/' . $ffname;
        }
        if ($request->hasFile('pan')) {
            $ffname = $request->file('pan')->getClientOriginalName();
            $fpath = $request->file('pan')->move('storage/uploads', $ffname);
            $path2 = $currentURL . '/' .'storage/uploads/' . $ffname;
        }
        if ($request->hasFile('incomep')) {
            $ffname = $request->file('incomep')->getClientOriginalName();
            $fpath = $request->file('incomep')->move('storage/uploads', $ffname);
            $path3 = $currentURL . '/' .'storage/uploads/' . $ffname;
        }





   
        $affectedRows = DB::table('applyloans')->insert([
            'loanid' => $randomID,
            'name' => $request->input('name'),
            'email' => Auth::user()->email,
            'phn' => Auth::user()->phone,
            'add' => $request->input('add'),
            'incometype' => $request->input('incometype'),
            'loan' => $request->input('loan'),
            'monthlyincome' => $request->input('monthlyincome'),
            'aadhar' => $path1,
            'pan' => $path2,
            'incomep' => $path3,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    
        return response()->json('success');
    }

    /**
     * Display the specified resource.
     */
    public function show(applyloan $applyloan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request)
    {
        DB::table('applyloans')->where('id', $request->id)->update([
            'status'=>1,
            'ammount' => $request->input('approvedAmount'), 
            'month' => $request->input('Month'), 
            'emi' => $request->input('Emi'),
            'updated_at' => now()

        ]);

        return response()->json('success');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id)
    {
        DB::table('applyloans')->where('id', $id)->update([
            'status'=>2,
           
            'updated_at' => now()

        ]);

        return response()->json('success');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::table('applyloans')->where('id', $id)->delete();
        return response()->json('success'); 
    }
}
