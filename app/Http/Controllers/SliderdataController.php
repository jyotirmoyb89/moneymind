<?php

namespace App\Http\Controllers;

use App\Models\sliderdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SliderdataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $res= DB::table('sliderdatas')->get();
       return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        dd($request->all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(sliderdata $sliderdata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'photo' => 'required|mimes:jpeg,png,gif,webp|max:2048', // Required image field with specific MIME types and file size limit
        ]);
    
        $path = '';
    
        if ($request->hasFile('photo')) {
            $ffname = $request->file('photo')->getClientOriginalName();
            $fpath = $request->file('photo')->move('storage/uploads', $ffname);
            $path = 'storage/uploads/' . $ffname;
        }
    
        // $data = [
        //     'name' => $request->input('content'),
        //     'photo' => $path,
        //     'created_at' => now(),
        //     'updated_at' => now()
        // ];
        $currentURL = url()->to('/');
        $pathWithServerURL = $currentURL . '/' . $path;

        DB::table('sliderdatas')->insert([
            'content' => $request->input('content'),
            'img' => $pathWithServerURL,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return response()->json('success');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, sliderdata $sliderdata)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $sliderdata = Sliderdata::findOrFail($id);
        $sliderdata->delete();

        return response()->json(['message' => 'Slider data deleted successfully']);
    }
}
